package com.eng.androidkursday23app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.BufferedReader
import java.io.InputStreamReader

class ModelsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_models)

        DataGlobal.dataHandler.filterCars(intent)

        val recyclerView = findViewById<RecyclerView>(R.id.modelRecyclerView)
        recyclerView.adapter = modelCellAdapter(intent.getIntegerArrayListExtra("LOGOARRAY")!!, intent.getStringArrayListExtra("BRANDARRAY")!!)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        findViewById<ImageView>(R.id.brandLogotypeImage).setImageResource(DataGlobal.selectedBrandImage)
    }

    override fun onBackPressed() {
        finish()
        startActivity(Intent(this, LogotypesActivity::class.java))
    }


}