package com.eng.androidkursday23app

import android.content.Context

object DataGlobal {
    val username = ""
    val password = ""
    var loggedIn = false

    var cars: ArrayList<Car> = arrayListOf()
    var filteredCars: ArrayList<Car> = arrayListOf()
    var headers: List<String> = listOf()
    var selectedBrandImage: Int = 0

    var brandsAndImages: ArrayList<BrandAndImage> = arrayListOf()

    val dataHandler: DataHandler = DataHandler()

    var isSearch: Boolean = false
}