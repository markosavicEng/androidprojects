package com.eng.androidkursday23app

import android.content.Context
import android.content.Intent
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.BufferedReader
import java.io.InputStreamReader

class DataHandler {
    fun addLogosToArray(logoArray: ArrayList<Int>) {
        logoArray.clear()
        logoArray.add(R.drawable.alfa_romeo)
        logoArray.add(R.drawable.audi)
        logoArray.add(R.drawable.citroen)
        logoArray.add(R.drawable.dacia)
        logoArray.add(R.drawable.fiat)
        logoArray.add(R.drawable.ford)
        logoArray.add(R.drawable.honda)
        logoArray.add(R.drawable.hyundai)
        logoArray.add(R.drawable.infiniti)
        logoArray.add(R.drawable.isuzu)
        logoArray.add(R.drawable.jeep)
        logoArray.add(R.drawable.lada)
        logoArray.add(R.drawable.mazda)
        logoArray.add(R.drawable.mercedes_benz)
        logoArray.add(R.drawable.mini)
        logoArray.add(R.drawable.mitsubishi)
        logoArray.add(R.drawable.nissan)
        logoArray.add(R.drawable.opel)
        logoArray.add(R.drawable.peugeot)
        logoArray.add(R.drawable.renault)
        logoArray.add(R.drawable.seat)
        logoArray.add(R.drawable.skoda)
        logoArray.add(R.drawable.smart)
        logoArray.add(R.drawable.subaru)
        logoArray.add(R.drawable.suzuki)
        logoArray.add(R.drawable.volkswagen)
        logoArray.add(R.drawable.volvo)
    }

    fun addBrandsToArray(brandArray: ArrayList<String>) {
        brandArray.clear()
        brandArray.add("Alfa Romeo")
        brandArray.add("Audi")
        brandArray.add("Citroen")
        brandArray.add("Dacia")
        brandArray.add("Fiat")
        brandArray.add("Ford")
        brandArray.add("Honda")
        brandArray.add("Hyundai")
        brandArray.add("Infiniti")
        brandArray.add("Isuzu")
        brandArray.add("Jeep")
        brandArray.add("Lada")
        brandArray.add("Mazda")
        brandArray.add("Mercedes-Benz")
        brandArray.add("Mini")
        brandArray.add("Mitsubishi")
        brandArray.add("Nissan")
        brandArray.add("Opel")
        brandArray.add("Peugeot")
        brandArray.add("Renault")
        brandArray.add("Seat")
        brandArray.add("Skoda")
        brandArray.add("Smart")
        brandArray.add("Subaru")
        brandArray.add("Suzuki")
        brandArray.add("Volkswagen")
        brandArray.add("Volvo")
    }

    fun addBrandsAndImagesToArray() {
        DataGlobal.brandsAndImages.clear()
        DataGlobal.brandsAndImages.add(BrandAndImage("Alfa Romeo", R.drawable.alfa_romeo_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Audi", R.drawable.audi_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Citroen", R.drawable.citroen_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Dacia", R.drawable.dacia_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Fiat", R.drawable.fiat_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Ford", R.drawable.ford_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Honda", R.drawable.honda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Hyundai", R.drawable.hyundai_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Infiniti", R.drawable.infiniti_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Isuzu", R.drawable.isuzu_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Jeep", R.drawable.jeep_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Lada", R.drawable.lada_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mazda", R.drawable.mazda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mercedes-Benz", R.drawable.mercedes_benz_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mini", R.drawable.mini_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mitsubishi", R.drawable.mitsubishi_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Nissan", R.drawable.nissan_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Opel", R.drawable.opel_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Peugeot", R.drawable.peugeot_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Renault", R.drawable.renault_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Seat", R.drawable.seat_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Skoda", R.drawable.skoda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Smart", R.drawable.smart_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Subaru", R.drawable.subaru_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Suzuki", R.drawable.suzuki_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Volkswagen", R.drawable.volkswagen_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Volvo", R.drawable.volvo_car))
    }

    fun parseCSVToArray(context: Context) {
        var isFirstRow = true
        var line: String?
        val readLine = BufferedReader(InputStreamReader(context.assets.open("modelExcelNew.csv")))

        while (readLine.readLine().also { line = it } != null) {
            val row: List<String> = line!!.split(";")
            if (isFirstRow) {
                DataGlobal.headers = row
                isFirstRow = false
            } else {
                DataGlobal.cars.add(Car(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],
                    row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19], row[20],
                    row[21], row[22], row[23], row[24], row[25], row[26], row[27], row[28], row[29], row[30], row[31],
                    row[32], row[33], row[34], row[35], row[36], row[37], row[38], row[39], row[40], row[41], row[42],
                    row[43], row[44], row[45], row[46], row[47], row[48], row[49], row[50], row[51], row[52], row[53],
                    row[54], row[55], row[56], row[57], row[58], row[59], row[60]))
            }
        }
    }

    fun search(view: View, context: Context, logoArray: ArrayList<Int>, brandArray: ArrayList<String>, s: String) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.logotypeRecyclerView)
        if (!s.equals("") && DataGlobal.cars.filter { s.lowercase().replace(" ", "") in (it.marka + it.model).lowercase().replace(" ", "")}.isNotEmpty()){
            DataGlobal.filteredCars.clear()
            DataGlobal.isSearch = true
            DataGlobal.filteredCars = DataGlobal.cars.filter { s.lowercase().replace(" ", "") in (it.marka.lowercase().replace(" ", "") + it.model.lowercase().replace(" ", "")) } as ArrayList<Car>
        } else {
            DataGlobal.filteredCars.clear()
            Toast.makeText(context, "Search was unsuccessfull!", Toast.LENGTH_SHORT).show()
        }
        recyclerView.adapter = modelCellAdapter(logoArray, brandArray)
        recyclerView.layoutManager = GridLayoutManager(context, 2)
    }

    fun filterCars(intent: Intent){
        DataGlobal.filteredCars.clear()
        for (car in DataGlobal.cars) {
            if (car.marka in intent.getStringExtra("BRAND")!!) {
                DataGlobal.filteredCars.add(car)
            }
        }
    }

    fun setModelDetailsViews(intent: Intent, view: View) {
        val selectedCar = DataGlobal.filteredCars.filter { it.marka == intent.getStringExtra("BRAND") && it.model == intent.getStringExtra("MODEL") && it.motor == intent.getStringExtra("ENGINE") && it.paketOpreme == intent.getStringExtra("PACKAGE") && it.tezinaPraznogVozila == intent.getStringExtra("WEIGHT")}[0]

        for (i in DataGlobal.brandsAndImages) {
            if (i.brand in intent.getStringExtra("BRAND")!!) {
                view.findViewById<ImageView>(R.id.modelDetailsModelImage).setImageResource(i.image)
            }
        }

        view.findViewById<TextView>(R.id.modelDetailsMarka).text = selectedCar.marka
        view.findViewById<TextView>(R.id.modelDetailsModel).text = selectedCar.model
        view.findViewById<TextView>(R.id.modelDetailsMotor).text = selectedCar.motor
        view.findViewById<TextView>(R.id.modelDetailsPaketOpreme).text = selectedCar.paketOpreme
        view.findViewById<TextView>(R.id.modelDetailsCena).text = selectedCar.cena + " \u20ac"
        view.findViewById<TextView>(R.id.modelDetailsRasporedCilindara).text = selectedCar.rasporedCilindara
        view.findViewById<TextView>(R.id.modelDetailsBrojVentila).text = selectedCar.brojVentila
        view.findViewById<TextView>(R.id.modelDetailsPrecnikHodKlipa).text = selectedCar.precnikHodKlipa
        view.findViewById<TextView>(R.id.modelDetailsTipUbrizgavanja).text = selectedCar.tipUbrizgavanja
        view.findViewById<TextView>(R.id.modelDetailsSistemOtvaranjaVentila).text = selectedCar.sistemOtvaranjaVentila
        view.findViewById<TextView>(R.id.modelDetailsTurbo).text = selectedCar.turbo
        view.findViewById<TextView>(R.id.modelDetailsZapreminaMotora).text = selectedCar.zapreminaMotora
        view.findViewById<TextView>(R.id.modelDetailsKW).text = selectedCar.KW
        view.findViewById<TextView>(R.id.modelDetailsKS).text = selectedCar.KS
        view.findViewById<TextView>(R.id.modelDetailsSnagaPriObrtajima).text = selectedCar.snagaPriObrtajima
        view.findViewById<TextView>(R.id.modelDetailsObrtniMoment).text = selectedCar.obrtniMoment
        view.findViewById<TextView>(R.id.modelDetailsObrtniMomentPriObrtajima).text = selectedCar.obrtniMomentPriObrtajima
        view.findViewById<TextView>(R.id.modelDetailsStepenKompresije).text = selectedCar.stepenKompresije
        view.findViewById<TextView>(R.id.modelDetailsTipMenjaca).text = selectedCar.tipMenjaca
        view.findViewById<TextView>(R.id.modelDetailsBrojStepeniPrenosa).text = selectedCar.brojStepeniPrenosa
        view.findViewById<TextView>(R.id.modelDetailsPogon).text = selectedCar.pogon
        view.findViewById<TextView>(R.id.modelDetailsDuzina).text = selectedCar.duzina
        view.findViewById<TextView>(R.id.modelDetailsSirina).text = selectedCar.sirina
        view.findViewById<TextView>(R.id.modelDetailsVisina).text = selectedCar.visina
        view.findViewById<TextView>(R.id.modelDetailsMedjuosovinskoRastojanje).text = selectedCar.medjuosovinskoRastojanje
        view.findViewById<TextView>(R.id.modelDetailsTezinaPraznogVozila).text = selectedCar.tezinaPraznogVozila
        view.findViewById<TextView>(R.id.modelDetailsMaksimalnaDozvoljenaTezina).text = selectedCar.maksimalnaDozvoljenaTezina
        view.findViewById<TextView>(R.id.modelDetailsZapreminaRezervoara).text = selectedCar.zapreminaRezervoara
        view.findViewById<TextView>(R.id.modelDetailsZapreminaPrtljaznika).text = selectedCar.zapreminaPrtljaznika
        view.findViewById<TextView>(R.id.modelDetailsMaksZapreminaPrtljaznika).text = selectedCar.maksZapreminaPrtljaznika
        view.findViewById<TextView>(R.id.modelDetailsDozvoljenTovar).text = selectedCar.dozvoljenTovar
        view.findViewById<TextView>(R.id.modelDetailsDozvoljenoOpterecenjeKrova).text = selectedCar.dozvoljenoOpterecenjeKrova
        view.findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceBK).text = selectedCar.dozvoljenaTezinaPrikoliceBK
        view.findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceSK12).text = selectedCar.dozvoljenaTezinaPrikoliceSK12
        view.findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceSK8).text = selectedCar.dozvoljenaTezinaPrikoliceSK8
        view.findViewById<TextView>(R.id.modelDetailsOpterecenjeKuke).text = selectedCar.opterecenjeKuke
        view.findViewById<TextView>(R.id.modelDetailsRadijusOkretanja).text = selectedCar.radijusOkretanja
        view.findViewById<TextView>(R.id.modelDetailsTragTockovaNapred).text = selectedCar.tragTockovaNapred
        view.findViewById<TextView>(R.id.modelDetailsTragTockovaNazad).text = selectedCar.tragTockovaNazad
        view.findViewById<TextView>(R.id.modelDetailsMaksimalnaBrzina).text = selectedCar.maksimalnaBrzina
        view.findViewById<TextView>(R.id.modelDetailsUbrzanje0_100).text = selectedCar.ubrzanje0_100
        view.findViewById<TextView>(R.id.modelDetailsUbrzanje0_200).text = selectedCar.ubrzanje0_200
        view.findViewById<TextView>(R.id.modelDetailsUbrzanje80_120FinalniStepen).text = selectedCar.ubrzanje80_120FinalniStepen
        view.findViewById<TextView>(R.id.modelDetailsZaustavniPut100).text = selectedCar.zaustavniPut100
        view.findViewById<TextView>(R.id.modelDetailsVremeZa400m).text = selectedCar.vremeZa400m
        view.findViewById<TextView>(R.id.modelDetailsPotrosnjaGrad).text = selectedCar.potrosnjaGrad
        view.findViewById<TextView>(R.id.modelDetailsPotrosnjaVGrada).text = selectedCar.potrosnjaVGrada
        view.findViewById<TextView>(R.id.modelDetailsEmisijaCO2).text = selectedCar.emisijaCO2
        view.findViewById<TextView>(R.id.modelDetailsKatalizator).text = selectedCar.katalizator
        view.findViewById<TextView>(R.id.modelDetailsDimenzijePenumatika).text = selectedCar.dimenzijePneumatika
        view.findViewById<TextView>(R.id.modelDetailsPrednjeOpruge).text = selectedCar.prednjeOpruge
        view.findViewById<TextView>(R.id.modelDetailsZadnjeOpruge).text = selectedCar.zadnjeOpruge
        view.findViewById<TextView>(R.id.modelDetailsPrednjiStabilizator).text = selectedCar.prednjiStabilizator
        view.findViewById<TextView>(R.id.modelDetailsZadnjiStabilizator).text = selectedCar.zadnjiStabilizator
        view.findViewById<TextView>(R.id.modelDetailsGarancijaKorozija).text = selectedCar.garancijaKorozija
        view.findViewById<TextView>(R.id.modelDetailsGarancijaMotor).text = selectedCar.garancijaMotor
        view.findViewById<TextView>(R.id.modelDetailsEuroNCAP).text = selectedCar.euroNCAP
        view.findViewById<TextView>(R.id.modelDetailsEuroNCAPZvezdice).text = selectedCar.euroNCAPZvezdice
        view.findViewById<TextView>(R.id.modelDetailsGorivo).text = selectedCar.gorivo
        view.findViewById<TextView>(R.id.modelDetailsBrojVrata).text = selectedCar.brojVrata
        view.findViewById<TextView>(R.id.modelDetailsBrojSedista).text = selectedCar.brojSedista

        view.findViewById<ImageView>(R.id.modelDetailsBrandLogotype).setImageResource(DataGlobal.selectedBrandImage)
    }

    fun handleDataModelCellAdapter(holder: CustomViewHolder, position: Int, logoArray: ArrayList<Int>, brandArray: ArrayList<String>) {
        var image = DataGlobal.brandsAndImages.filter { it.brand in DataGlobal.filteredCars[position].marka }[0].image
        holder.view.findViewById<ImageView>(R.id.modelImage).setImageResource(image)
        holder.view.findViewById<TextView>(R.id.modelNameText).text = DataGlobal.filteredCars[position].marka + " " + DataGlobal.filteredCars[position].model
        holder.view.findViewById<TextView>(R.id.priceText).text = DataGlobal.filteredCars[position].cena + " \u20ac"

        holder.view.setOnClickListener {

            val intent = Intent(holder.view.context, ModelDetailsActivity::class.java)
            intent.putExtra("BRAND", DataGlobal.filteredCars[position].marka)
            intent.putExtra("MODEL", DataGlobal.filteredCars[position].model)
            intent.putExtra("ENGINE", DataGlobal.filteredCars[position].motor)
            intent.putExtra("PACKAGE", DataGlobal.filteredCars[position].paketOpreme)
            intent.putExtra("WEIGHT", DataGlobal.filteredCars[position].tezinaPraznogVozila)

            DataGlobal.selectedBrandImage = logoArray[brandArray.indexOf(DataGlobal.filteredCars[position].marka.trim())]
            holder.view.context.startActivity(intent)
        }
    }

    fun handleDataLogotypeCellAdapter(holder: CustomViewHolder, position: Int, logoArray: ArrayList<Int>, brandArray: ArrayList<String>) {
        holder.view.findViewById<ImageView>(R.id.cellImage).setImageResource(logoArray[position])
        holder.view.findViewById<TextView>(R.id.cellText).text = brandArray[position]

        holder.view.setOnClickListener {
            val intent = Intent(holder.view.context, ModelsActivity::class.java)
            intent.putExtra("BRAND", brandArray[position])

            intent.putExtra("LOGOARRAY", logoArray)
            intent.putExtra("BRANDARRAY", brandArray)

            DataGlobal.selectedBrandImage = logoArray[position]
            holder.view.context.startActivity(intent)
        }
    }
}