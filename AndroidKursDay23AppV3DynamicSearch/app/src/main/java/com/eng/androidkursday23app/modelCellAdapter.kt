package com.eng.androidkursday23app

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class modelCellAdapter(val logoArray: ArrayList<Int>, val brandArray: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.model_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        DataGlobal.dataHandler.handleDataModelCellAdapter(holder, position, logoArray, brandArray)
    }

    override fun getItemCount(): Int {
        return DataGlobal.filteredCars.count()
    }

}