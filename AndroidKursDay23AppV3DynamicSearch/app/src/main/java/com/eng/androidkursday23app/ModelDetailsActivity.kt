package com.eng.androidkursday23app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout

class ModelDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model_details)

        DataGlobal.dataHandler.setModelDetailsViews(intent, findViewById(R.id.modelDetailsActivityLayout))

        val showDetails = findViewById<ConstraintLayout>(R.id.showDetails)
        val hideDetails = findViewById<ConstraintLayout>(R.id.hideDetails)
        val hiddenAttributes = findViewById<ConstraintLayout>(R.id.hiddenAttributes)

        showDetails.setOnClickListener {
            hiddenAttributes.visibility = View.VISIBLE
            showDetails.visibility = View.GONE
            hideDetails.visibility = View.VISIBLE
        }

        hideDetails.setOnClickListener {
            hiddenAttributes.visibility = View.GONE
            showDetails.visibility = View.VISIBLE
            hideDetails.visibility = View.GONE
        }
    }

}