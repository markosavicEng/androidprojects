package com.eng.androidkursday23app

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.content.res.AssetManager
import android.text.Editable
import android.text.TextWatcher

data class BrandAndImage(val brand: String, val image: Int)

class LogotypesActivity : AppCompatActivity() {

    var logoArray: ArrayList<Int> = arrayListOf()
    var brandArray: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotypes)

        DataGlobal.isSearch = false

        DataGlobal.filteredCars.clear()

        DataGlobal.dataHandler.addLogosToArray(logoArray)
        DataGlobal.dataHandler.addBrandsToArray(brandArray)
        DataGlobal.dataHandler.addBrandsAndImagesToArray()

        val recyclerView = findViewById<RecyclerView>(R.id.logotypeRecyclerView)

        val context: Context = this
        findViewById<EditText>(R.id.searchBarEditText).addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                DataGlobal.dataHandler.search(findViewById(R.id.logotypesLayout), context, logoArray, brandArray, s.toString())
            }

        })

        recyclerView.adapter = logotypeCellAdapter(logoArray, brandArray)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
    }

    override fun onBackPressed() {
        finish()
        if(DataGlobal.isSearch)
            startActivity(Intent(this, LogotypesActivity::class.java))
        else
            startActivity(Intent(this, MainActivity::class.java))
    }
}