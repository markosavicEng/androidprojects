package com.eng.androidkursday29task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    var weatherDataArray: ArrayList<WeatherData> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataGlobal.setIcons()
        readJSON()

        val adapter = WeatherCellAdapter(weatherDataArray)
        val recyclerView = findViewById<RecyclerView>(R.id.weatherRecyclerView)
        recyclerView.adapter = adapter
        recyclerView.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
    }

    fun readJSON() {
        val json: String?

        try {
            val inputStream: InputStream = assets.open("weather.json")
            json = inputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val weatherDataObject = jsonArray.getJSONObject(it)
                    weatherDataArray.add(WeatherData(
                            weatherDataObject.getJSONObject("coord"),
                            weatherDataObject.getJSONArray("weather"),
                            weatherDataObject.getString("base"),
                            weatherDataObject.getJSONObject("main"),
                            weatherDataObject.getInt("visibility"),
                            weatherDataObject.getJSONObject("wind"),
                            weatherDataObject.getJSONObject("clouds"),
                            weatherDataObject.getInt("dt"),
                            weatherDataObject.getJSONObject("sys"),
                            weatherDataObject.getInt("timezone"),
                            weatherDataObject.getInt("id"),
                            weatherDataObject.getString("name"),
                            weatherDataObject.getInt("cod"))
                    )
                }
            }
        } catch (e: IOException) {
            Log.d("JSON Exception", e.toString())
        }
    }
}

data class Icon(val description: String, val icon: Int)

data class WeatherData(
    val coord: JSONObject,
    val weather: JSONArray,
    val base: String,
    val main: JSONObject,
    val visibility: Int,
    val wind: JSONObject,
    val clouds: JSONObject,
    val dt: Int,
    val sys: JSONObject,
    val timezone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)

//data class Coord(
//    val lon: Double,
//    val lat: Double
//)
//
//data class Weather(
//    val id: Int,
//    val main: String,
//    val description: String,
//    val icon: String
//)
//
//data class Main(
//    val temp: Double,
//    val feelsLike: Double,
//    val tempMin: Double,
//    val tempMax: Double,
//    val pressure: Int,
//    val humidity: Int
//)
//
//data class Wind(
//    val speed: Double,
//    val deg: Double,
//    val gust: Double
//)
//
//data class Clouds(
//    val all: Int
//)
//
//data class Sys(
//    val type: Int,
//    val id: Int,
//    val country: String,
//    val sunrise: Int,
//    val sunset: Int
//)