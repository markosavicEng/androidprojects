package com.eng.androidkursday29task01

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject

class WeatherCellAdapter(val weatherDataArray: ArrayList<WeatherData>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.weather_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.cityTextView).text = weatherDataArray[position].name
        holder.view.findViewById<TextView>(R.id.countryTextView).text = "(" + weatherDataArray[position].sys.getString("country") + ")"
        holder.view.findViewById<TextView>(R.id.timezoneTextView).text = "Timezone: " + weatherDataArray[position].timezone
        holder.view.findViewById<TextView>(R.id.latitudeTextView).text = "Lat: " + weatherDataArray[position].coord.getDouble("lat") + " / "
        holder.view.findViewById<TextView>(R.id.longitudeTextView).text = "Lon: " + weatherDataArray[position].coord.getDouble("lon")
        holder.view.findViewById<TextView>(R.id.tempTextView).text = weatherDataArray[position].main.getDouble("temp").toString() + " K"
        holder.view.findViewById<TextView>(R.id.pressureTextView).text = "Pressure: " + weatherDataArray[position].main.getDouble("pressure").toString() + " mbar"
        holder.view.findViewById<TextView>(R.id.humidityTextView).text = "Humidity: " + weatherDataArray[position].main.getInt("humidity").toString() + " %"
        holder.view.findViewById<TextView>(R.id.visibilityTextView).text = "Visibility: " + weatherDataArray[position].visibility + " mi"
        holder.view.findViewById<TextView>(R.id.windSpeedTextView).text = "Wind speed: " + weatherDataArray[position].wind.getDouble("speed") + " m/s"
        holder.view.findViewById<TextView>(R.id.windDegTextView).text = "Wind direction: " + weatherDataArray[position].wind.getInt("deg") + " \u00b0"
        holder.view.findViewById<TextView>(R.id.windGustTextView).text = "Wind gust: " + weatherDataArray[position].wind.getDouble("gust") + " m/s"
        holder.view.findViewById<TextView>(R.id.cloudsTextView).text = "Clouds: " + weatherDataArray[position].clouds.getInt("all") + " %"

        var weatherDataArrayList: ArrayList<JSONObject> = arrayListOf()
        weatherDataArray[position].weather?.let {
            (0 until it.length()).forEach {
                weatherDataArrayList.add(weatherDataArray[position].weather.getJSONObject(it))
            }
        }

        val adapter = WeatherIconCellAdapter(weatherDataArrayList)
        val recyclerView = holder.view.findViewById<RecyclerView>(R.id.weatherIconRecyclerView)
        recyclerView.adapter = adapter
        recyclerView.setLayoutManager(LinearLayoutManager(holder.view.context, LinearLayoutManager.HORIZONTAL, false))
    }

    override fun getItemCount(): Int {
        return weatherDataArray.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view)