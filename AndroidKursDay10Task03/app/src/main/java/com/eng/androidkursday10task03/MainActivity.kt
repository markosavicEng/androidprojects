package com.eng.androidkursday10task03

import android.R.attr.left
import android.R.attr.right
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        signinButton.setOnClickListener {
            usernamelayout.visibility = View.VISIBLE
            username.visibility = View.VISIBLE
            passwordlayout.visibility = View.VISIBLE
            password.visibility = View.VISIBLE
            forgotpasswordlayout.visibility = View.VISIBLE
            or.visibility = View.VISIBLE
            socialmedia.visibility = View.VISIBLE
            header.visibility = View.GONE
            signinButton.visibility = View.GONE
            signupButton.text = "LOG IN"
            signupButton.setBackgroundColor(resources.getColor(R.color.orange))
        }
    }
}