package com.eng.androidkursday29task01

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class AlertActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        DataGlobal.alertBackPressed = true
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}