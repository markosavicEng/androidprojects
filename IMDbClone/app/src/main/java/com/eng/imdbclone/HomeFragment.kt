package com.eng.imdbclone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class HomeFragment : Fragment() {

    var fragmentView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        println("POKRECE SE FRAGMENT")
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        if (!DataGlobal.appStarted) {
            DataGlobal.addMoviesToLists()
            DataGlobal.appStarted = true
        }

        createMoviesFromIDs()

        fragmentView = view
        view.findViewById<TextView>(R.id.plusbutton).setOnClickListener {
            Log.d("allthebest: ",
                DataGlobal.allTheBestMoviesComingThisSummerMovieList.count().toString()
            )
            Log.d("upcoming: ",
                DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count().toString()
            )
            Log.d("whattvshows: ",
                DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count().toString()
            )
            Log.d("allthelatest: ",
                DataGlobal.allTheLatestMovieAndTVPostersMovieList.count().toString()
            )
        }
        setFeaturedTodayImages()
        setFanFavoritesData()

        return view
    }

    fun createMoviesFromIDs() {
        for (i in DataGlobal.allTheBestMoviesComingThisSummer) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("allthebestmovies: ${i}")
            loadIMDbDataJSON(DataGlobal.allTheBestMoviesComingThisSummerMovieList).execute(url)
        }

        for (i in DataGlobal.upcomingHorrorMoviesWeCantWaitToSee) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("upcoming: ${i}")
            loadIMDbDataJSON(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList).execute(url)
        }

        for (i in DataGlobal.whatTVShowsAreRenewedOrCanceled) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("whattvshows: ${i}")
            loadIMDbDataJSON(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList).execute(url)
        }

        for (i in DataGlobal.allTheLatestMovieAndTVPosters) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            println("allthelatest: ${i}")
            loadIMDbDataJSON(DataGlobal.allTheLatestMovieAndTVPostersMovieList).execute(url)
        }

        for (i in DataGlobal.fanFavorites) {
            val url = "https://www.omdbapi.com/?i=${i}&apikey=64072e92"
            loadIMDbDataJSON(DataGlobal.fanFavoritesMovieList).execute(url)
        }
//        setFeaturedTodayImages()
//        setFanFavoritesData()
    }

    fun setFanFavoritesData() {
        println("POKRECE SE ADAPTER U FRAGMENT")
        val adapter = MovieCell1Adapter(DataGlobal.fanFavoritesMovieList)
        val recyclerView = fragmentView?.findViewById<RecyclerView>(R.id.fanFavoritesRecyclerView)
        recyclerView?.adapter = adapter
        recyclerView?.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false))
    }

    fun createMovie(jsonObject: JSONObject, movieRatingList: ArrayList<MovieRating>, hasNull: Boolean) : Movie {
        if (hasNull) {
            return Movie(
                jsonObject.getString("Title"),
                jsonObject.getString("Year"),
                jsonObject.getString("Rated"),
                jsonObject.getString("Released"),
                jsonObject.getString("Runtime"),
                jsonObject.getString("Genre"),
                jsonObject.getString("Director"),
                jsonObject.getString("Writer"),
                jsonObject.getString("Actors"),
                jsonObject.getString("Plot"),
                jsonObject.getString("Language"),
                jsonObject.getString("Country"),
                jsonObject.getString("Awards"),
                jsonObject.getString("Poster"),
                movieRatingList,
                jsonObject.getString("Metascore"),
                jsonObject.getString("imdbRating"),
                jsonObject.getString("imdbVotes"),
                jsonObject.getString("imdbID"),
                jsonObject.getString("Type"),
                DVD = null,
                boxOffice = null,
                production = null,
                website = null,
                jsonObject.getString("Response")
            )
        } else {
            return Movie(
                jsonObject.getString("Title"),
                jsonObject.getString("Year"),
                jsonObject.getString("Rated"),
                jsonObject.getString("Released"),
                jsonObject.getString("Runtime"),
                jsonObject.getString("Genre"),
                jsonObject.getString("Director"),
                jsonObject.getString("Writer"),
                jsonObject.getString("Actors"),
                jsonObject.getString("Plot"),
                jsonObject.getString("Language"),
                jsonObject.getString("Country"),
                jsonObject.getString("Awards"),
                jsonObject.getString("Poster"),
                movieRatingList,
                jsonObject.getString("Metascore"),
                jsonObject.getString("imdbRating"),
                jsonObject.getString("imdbVotes"),
                jsonObject.getString("imdbID"),
                jsonObject.getString("Type"),
                jsonObject.getString("DVD"),
                jsonObject.getString("BoxOffice"),
                jsonObject.getString("Production"),
                jsonObject.getString("Website"),
                jsonObject.getString("Response")
            )
        }
    }

    inner class loadIMDbDataJSON(var currentMovieList: ArrayList<Movie>) : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val jsonObject = JSONObject(result!!)

            var movieRatingList:ArrayList<MovieRating> = arrayListOf()
            println(jsonObject)
            if (jsonObject.getJSONArray("Ratings").length() != 0) {
                jsonObject.getJSONArray("Ratings").let {
                    (0 until jsonObject.getJSONArray("Ratings").length()).forEach {
                        movieRatingList.add(
                            MovieRating(
                                jsonObject.getJSONArray("Ratings").getJSONObject(it)
                                    .getString("Source"),
                                jsonObject.getJSONArray("Ratings").getJSONObject(it)
                                    .getString("Value")
                            )
                        )
                    }
                }
            } else movieRatingList.add(MovieRating("", ""))

            Log.d("ID", DataGlobal.currentIDArray.toString())

            if (jsonObject.getString("Type") == "movie") {
                currentMovieList.add(
                    createMovie(jsonObject, movieRatingList, false)
                )
            } else {
                currentMovieList.add(
                    createMovie(jsonObject, movieRatingList, true)
                )
            }

            setFeaturedTodayImages()

            // if this is the last element (run only at the end of the last loop)
            if (DataGlobal.fanFavorites.count() == DataGlobal.fanFavoritesMovieList.count()) {
                setFanFavoritesData()
            }
        }
    }

    inner class loadImageFromURL(val imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val url = params[0]

            try {
                val imageFromAPI = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromAPI)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            imageView.setImageBitmap(result)
        }
    }

    fun setFeaturedTodayImages() {
        println("POSTAVLJAJU SE SLIKE")
        println(DataGlobal.allTheBestMoviesComingThisSummerMovieList.count())


        loadImageFromURL(fragmentView!!.findViewById(R.id.image2_1)).execute(DataGlobal.beforeTheyWereFamousImage1URL)
        loadImageFromURL(fragmentView!!.findViewById(R.id.image2_2)).execute(DataGlobal.beforeTheyWereFamousImage2URL)

        loadImageFromURL(fragmentView!!.findViewById(R.id.image4_1)).execute(DataGlobal.reflectedOnScreenImage1URL)

        loadImageFromURL(fragmentView!!.findViewById(R.id.image6_1)).execute(DataGlobal.theLatestRedCarpetPhotosAndMoreImage1URL)
        loadImageFromURL(fragmentView!!.findViewById(R.id.image6_2)).execute(DataGlobal.theLatestRedCarpetPhotosAndMoreImage2URL)

        loadImageFromURL(fragmentView!!.findViewById(R.id.image7_1)).execute(DataGlobal.celebratePrideImage1URL)

        loadImageFromURL(fragmentView!!.findViewById(R.id.image9_1)).execute(DataGlobal.rememberingNedBeattyImage1URL)
        loadImageFromURL(fragmentView!!.findViewById(R.id.image9_2)).execute(DataGlobal.rememberingNedBeattyImage2URL)

        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == DataGlobal.allTheBestMoviesComingThisSummerImage1Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_1)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[DataGlobal.allTheBestMoviesComingThisSummerImage1Index].poster)
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == DataGlobal.allTheBestMoviesComingThisSummerImage2Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_2)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[DataGlobal.allTheBestMoviesComingThisSummerImage2Index].poster)
        if (DataGlobal.allTheBestMoviesComingThisSummerMovieList.count() == DataGlobal.allTheBestMoviesComingThisSummerImage3Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image1_3)).execute(DataGlobal.allTheBestMoviesComingThisSummerMovieList[DataGlobal.allTheBestMoviesComingThisSummerImage3Index].poster)

        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage1Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image3_1)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage1Index].poster)
        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage2Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image3_2)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage2Index].poster)
        if (DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList.count() == DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage3Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image3_3)).execute(DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeMovieList[DataGlobal.upcomingHorrorMoviesWeCantWaitToSeeImage3Index].poster)

        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == DataGlobal.whatTVShowsAreRenewedOrCanceledImage1Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image5_1)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[DataGlobal.whatTVShowsAreRenewedOrCanceledImage1Index].poster)
        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == DataGlobal.whatTVShowsAreRenewedOrCanceledImage2Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image5_2)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[DataGlobal.whatTVShowsAreRenewedOrCanceledImage2Index].poster)
        if (DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList.count() == DataGlobal.whatTVShowsAreRenewedOrCanceledImage3Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image5_3)).execute(DataGlobal.whatTVShowsAreRenewedOrCanceledMovieList[DataGlobal.whatTVShowsAreRenewedOrCanceledImage3Index].poster)

        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == DataGlobal.allTheLatestMovieAndTVPostersImage1Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image8_1)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[DataGlobal.allTheLatestMovieAndTVPostersImage1Index].poster)
        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == DataGlobal.allTheLatestMovieAndTVPostersImage2Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image8_2)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[DataGlobal.allTheLatestMovieAndTVPostersImage2Index].poster)
        if (DataGlobal.allTheLatestMovieAndTVPostersMovieList.count() == DataGlobal.allTheLatestMovieAndTVPostersImage3Index + 1)
            loadImageFromURL(fragmentView!!.findViewById(R.id.image8_3)).execute(DataGlobal.allTheLatestMovieAndTVPostersMovieList[DataGlobal.allTheLatestMovieAndTVPostersImage3Index].poster)

    }
}