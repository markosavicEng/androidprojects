package com.eng.imdbclone

object DataGlobal {

    var appStarted = false

    var currentIDArray = 1

    var allTheBestMoviesComingThisSummer: ArrayList<String> = arrayListOf()
    var upcomingHorrorMoviesWeCantWaitToSee: ArrayList<String> = arrayListOf()
    var whatTVShowsAreRenewedOrCanceled: ArrayList<String> = arrayListOf()
    var allTheLatestMovieAndTVPosters: ArrayList<String> = arrayListOf()
    var fanFavorites: ArrayList<String> = arrayListOf()

    var allTheBestMoviesComingThisSummerMovieList: ArrayList<Movie> = arrayListOf()
    var upcomingHorrorMoviesWeCantWaitToSeeMovieList: ArrayList<Movie> = arrayListOf()
    var whatTVShowsAreRenewedOrCanceledMovieList: ArrayList<Movie> = arrayListOf()
    var allTheLatestMovieAndTVPostersMovieList: ArrayList<Movie> = arrayListOf()
    var fanFavoritesMovieList: ArrayList<Movie> = arrayListOf()

    fun addMoviesToLists() {
        allTheBestMoviesComingThisSummer.add("tt3228774")
        allTheBestMoviesComingThisSummer.add("tt8332922")
        allTheBestMoviesComingThisSummer.add("tt7069210")
        allTheBestMoviesComingThisSummer.add("tt6654210")
        allTheBestMoviesComingThisSummer.add("tt10418662")
        allTheBestMoviesComingThisSummer.add("tt1321510")
        allTheBestMoviesComingThisSummer.add("tt8385148")
        allTheBestMoviesComingThisSummer.add("tt12801262")
        allTheBestMoviesComingThisSummer.add("tt5433138")
        allTheBestMoviesComingThisSummer.add("tt10096842")
        allTheBestMoviesComingThisSummer.add("tt5439812")
        allTheBestMoviesComingThisSummer.add("tt9777666")
        allTheBestMoviesComingThisSummer.add("tt11422728")
        allTheBestMoviesComingThisSummer.add("tt6566576")
        allTheBestMoviesComingThisSummer.add("tt10327252")
        allTheBestMoviesComingThisSummer.add("tt9288692")
        allTheBestMoviesComingThisSummer.add("tt3480822")
        allTheBestMoviesComingThisSummer.add("tt3554046")
        allTheBestMoviesComingThisSummer.add("tt8368408")
        allTheBestMoviesComingThisSummer.add("tt10954652")
        allTheBestMoviesComingThisSummer.add("tt0870154")
        allTheBestMoviesComingThisSummer.add("tt9243804")
        allTheBestMoviesComingThisSummer.add("tt10696896")
        allTheBestMoviesComingThisSummer.add("tt6402468")
        allTheBestMoviesComingThisSummer.add("tt6334354")
        allTheBestMoviesComingThisSummer.add("tt2452150")
        allTheBestMoviesComingThisSummer.add("tt6246322")
        allTheBestMoviesComingThisSummer.add("tt10366460")
        allTheBestMoviesComingThisSummer.add("tt6264654")
        allTheBestMoviesComingThisSummer.add("tt9731534")
        allTheBestMoviesComingThisSummer.add("tt10731768")
        allTheBestMoviesComingThisSummer.add("tt3272066")
        allTheBestMoviesComingThisSummer.add("tt9347730")
        allTheBestMoviesComingThisSummer.add("tt10230994")
        allTheBestMoviesComingThisSummer.add("tt9735318")

        upcomingHorrorMoviesWeCantWaitToSee.add("tt10342730")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt9352356")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt11252440")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt0993840")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt8332922")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt7069210")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt10327252")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt9288692")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt10954652")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt6402468")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt6246322")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt9731534")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt10230994")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt9347730")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt3811906")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt10665338")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt9639470")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt7740510")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt6455162")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt11755740")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt8150814")
        upcomingHorrorMoviesWeCantWaitToSee.add("tt6566576")

        whatTVShowsAreRenewedOrCanceled.add("tt11083696")
        whatTVShowsAreRenewedOrCanceled.add("tt8421350")
        whatTVShowsAreRenewedOrCanceled.add("tt11650492")
        whatTVShowsAreRenewedOrCanceled.add("tt7949218")
        whatTVShowsAreRenewedOrCanceled.add("tt10314462")
        whatTVShowsAreRenewedOrCanceled.add("tt11815682")
        whatTVShowsAreRenewedOrCanceled.add("tt10726424")
        whatTVShowsAreRenewedOrCanceled.add("tt2403776")
        whatTVShowsAreRenewedOrCanceled.add("tt5774002")
        whatTVShowsAreRenewedOrCanceled.add("tt7187044")
        whatTVShowsAreRenewedOrCanceled.add("tt11041132")
        whatTVShowsAreRenewedOrCanceled.add("tt0184122")
        whatTVShowsAreRenewedOrCanceled.add("tt11640020")
        whatTVShowsAreRenewedOrCanceled.add("tt8129450")
        whatTVShowsAreRenewedOrCanceled.add("tt10276062")

        allTheLatestMovieAndTVPosters.add("tt9243804")
        allTheLatestMovieAndTVPosters.add("tt1697800")
        allTheLatestMovieAndTVPosters.add("tt6566576")
        allTheLatestMovieAndTVPosters.add("tt7504818")
        allTheLatestMovieAndTVPosters.add("tt9900092")
        allTheLatestMovieAndTVPosters.add("tt10814438")
        allTheLatestMovieAndTVPosters.add("tt6654210")
        allTheLatestMovieAndTVPosters.add("tt10653784")
        allTheLatestMovieAndTVPosters.add("tt9777666")
        allTheLatestMovieAndTVPosters.add("tt8531222")

        fanFavorites.add("tt9140554")
        fanFavorites.add("tt3228774")
        fanFavorites.add("tt10155688")
        fanFavorites.add("tt11083552")
        fanFavorites.add("tt8332922")
        fanFavorites.add("tt12809988")
        fanFavorites.add("tt1321510")
        fanFavorites.add("tt9544034")
        fanFavorites.add("tt11337862")
        fanFavorites.add("tt5109280")
        fanFavorites.add("tt7888964")
        fanFavorites.add("tt14544192")
        fanFavorites.add("tt6644200")
        fanFavorites.add("tt0903747")
        fanFavorites.add("tt9208876")
        fanFavorites.add("tt0944947")
        fanFavorites.add("tt0111161")
        fanFavorites.add("tt6741278")
        fanFavorites.add("tt2531336")
        fanFavorites.add("tt4154796")
        fanFavorites.add("tt6723592")
        fanFavorites.add("tt12361974")
        fanFavorites.add("tt3228774")
        fanFavorites.add("tt7286456")
        fanFavorites.add("tt0108778")
        fanFavorites.add("tt9140560")
        fanFavorites.add("tt13304410")
        fanFavorites.add("tt0468569")
        fanFavorites.add("tt12392504")
        fanFavorites.add("tt7979580")
        fanFavorites.add("tt0816692")
        fanFavorites.add("tt10541088")
        fanFavorites.add("tt14392248")
        fanFavorites.add("tt10272386")
        fanFavorites.add("tt1375666")
        fanFavorites.add("tt6751668")
        fanFavorites.add("tt4574334")
        fanFavorites.add("tt9770150")
        fanFavorites.add("tt4052886")
        fanFavorites.add("tt10048342")
        fanFavorites.add("tt1457767")
        fanFavorites.add("tt0109830")
        fanFavorites.add("tt4154756")
        fanFavorites.add("tt0137523")
        fanFavorites.add("tt6468322")
        fanFavorites.add("tt9179096")
        fanFavorites.add("tt5562070")
        fanFavorites.add("tt2948372")
        fanFavorites.add("tt0068646")
        fanFavorites.add("tt7366338")
        fanFavorites.add("tt1190634")
    }


    /* CONST VALUES */

    val allTheBestMoviesComingThisSummerImage1Index = 5
    val allTheBestMoviesComingThisSummerImage2Index = 17
    val allTheBestMoviesComingThisSummerImage3Index = 24

    val beforeTheyWereFamousImage1URL = "https://m.media-amazon.com/images/M/MV5BMTM4MTA4MDc2MF5BMl5BanBnXkFtZTcwNTE1MTcxOA@@._V1_FMjpg_UX480_.jpg"
    val beforeTheyWereFamousImage2URL = "https://m.media-amazon.com/images/M/MV5BMjc0NDkwMzY0M15BMl5BanBnXkFtZTgwNzkyMzgxMzI@._V1_FMjpg_UX600_.jpg"

    val upcomingHorrorMoviesWeCantWaitToSeeImage1Index = 1
    val upcomingHorrorMoviesWeCantWaitToSeeImage2Index = 8
    val upcomingHorrorMoviesWeCantWaitToSeeImage3Index = 16

    val reflectedOnScreenImage1URL  = "https://m.media-amazon.com/images/M/MV5BZTVlYjhhZTAtOWM5Mi00M2E1LWI5ZGUtODU4Yzc2YzQ4N2FkXkEyXkFqcGdeQWplZmZscA@@._V1_SP330,330,0,C,0,0,0_CR65,90,200,150_PIimdb-blackband-204-14,TopLeft,0,0_PIimdb-blackband-204-28,BottomLeft,0,1_CR0,0,200,150_PIimdb-bluebutton-big,BottomRight,-1,-1_ZAClip,4,123,16,196,verdenab,8,255,255,255,1_ZAon%2520IMDb,4,1,14,196,verdenab,7,255,255,255,1_ZA06%253A26,164,1,14,36,verdenab,7,255,255,255,1_PIimdb-HDIconMiniWhite,BottomLeft,4,-2_ZAReflected%2520on%2520Screen%253A%2520C%252E%252E%252E%2520%2528S4%252EE21%2529,24,138,14,176,arialbd,7,255,255,255,1_.jpg"

    val whatTVShowsAreRenewedOrCanceledImage1Index = 3
    val whatTVShowsAreRenewedOrCanceledImage2Index = 10
    val whatTVShowsAreRenewedOrCanceledImage3Index = 12

    val theLatestRedCarpetPhotosAndMoreImage1URL = "https://m.media-amazon.com/images/M/MV5BMTYzODU2OTI2Nl5BMl5BanBnXkFtZTgwMTUyMTAwMzE@._V1_.jpg"
    val theLatestRedCarpetPhotosAndMoreImage2URL = "https://static01.nyt.com/images/2020/02/09/fashion/carpet-2020-3208/carpet-2020-3208-mobileMasterAt3x-v3.jpg"

    val celebratePrideImage1URL = "https://m.media-amazon.com/images/M/MV5BNDhmMjg4NzEtYTM3Yy00ZjQyLWFkNWEtODhmMWRhODdkYmNmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg"

    val allTheLatestMovieAndTVPostersImage1Index = 2
    val allTheLatestMovieAndTVPostersImage2Index = 3
    val allTheLatestMovieAndTVPostersImage3Index = 5

    val rememberingNedBeattyImage1URL = "https://m.media-amazon.com/images/M/MV5BMjMwMDcyNjc0M15BMl5BanBnXkFtZTgwMzI0MDY1MTE@._V1_.jpg"
    val rememberingNedBeattyImage2URL = "https://imagez.tmz.com/image/83/o/2021/06/13/83ca2c523360429da8e3c351faf316e5_lg.jpg"
}