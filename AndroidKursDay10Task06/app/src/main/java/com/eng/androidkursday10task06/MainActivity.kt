package com.eng.androidkursday10task06

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imagebutton.setOnClickListener {
            progressBar.visibility = View.VISIBLE
        }

        registracija.setOnClickListener {
            if (ime.text.toString().trim().isNotEmpty() && prezime.text.toString().trim().isNotEmpty() && email.text.toString().trim().isNotEmpty() && lozinka.text.toString().trim().isNotEmpty())
                progressBar.visibility = View.GONE
        }
    }
}