package com.eng.androidkursday23app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class logotypeCellAdapter(val logoArray: ArrayList<Int>, val brandArray: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.logotype_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        DataGlobal.dataHandler.handleDataLogotypeCellAdapter(holder, position, logoArray, brandArray)
    }

    override fun getItemCount(): Int {
        return logoArray.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view)