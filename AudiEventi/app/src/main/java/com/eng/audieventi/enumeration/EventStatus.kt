package com.eng.audieventi.enumeration

enum class EventStatus (val value: String) {
    I("I"), // Inactive
    A("A"), // Active
    S("S") // Expired
}