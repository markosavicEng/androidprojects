package com.eng.audieventi.singleton

import com.eng.audieventi.model.*

object DataGlobal {

    var isPremiumUser: Boolean = false

    val publicEventsList: ArrayList<PublicEvent> = arrayListOf()
    val premiumEventsList: ArrayList<PremiumEvent> = arrayListOf()
    val pushNotificationList: ArrayList<PushNotification> = arrayListOf()
    val pushNotificationShortList: ArrayList<PushNotification> = arrayListOf()


    val publicEventsAndImagesList: ArrayList<PublicEventAndImage> = arrayListOf()
    val premiumEventsAndImagesList: ArrayList<PremiumEventAndImage> = arrayListOf()
    var foodExperienceAndImage: FoodExperienceAndImage? = null
    var placeAndImageAndCarousel: PlaceAndImageAndCarousel? = null
    var audiDriveExperienceAndImageAndCarousel: AudiDriveExperienceAndImageAndCarousel? = null
    var eventContactAndImage: EventContactAndImage? = null
}