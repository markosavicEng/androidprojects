package com.eng.audieventi.enumeration

enum class CouponStatus (val value: String) {
    INACTIVE("INACTIVE"),
    ACTIVE("ACTIVE"),
    USED("USED")
}