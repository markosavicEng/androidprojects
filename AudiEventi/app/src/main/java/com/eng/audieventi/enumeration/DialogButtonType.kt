package com.eng.audieventi.enumeration

enum class DialogButtonType {
    PROCEED,
    CLOSE
}