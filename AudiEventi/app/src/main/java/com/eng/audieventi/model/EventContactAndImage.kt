package com.eng.audieventi.model

import android.graphics.Bitmap

class EventContactAndImage(
    val eventContact: EventContact,
    val image: Bitmap?
)