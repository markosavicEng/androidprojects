package com.eng.audieventi.model

data class FoodExperience(
    val id: String,
    val title: String,
    val header: String,
    val foodSubtitle: FoodSubtitle,
    val foodImage: BackgroundImage,
    val experienceProgram: ArrayList<DailyFoodExperience>
)

/* TODO Check if FoodSubtitle and ProgramNotes (PremiumEvent.kt) should implement a common interface */
data class FoodSubtitle(
    val value: String,
    val format: String,
    val processed: String
)

data class DailyFoodExperience(
    val day: String,
    val start: String,
    val type: String,
    val activity: String,
    val site: String,
    val description: String,
    val food: String,
    val allergens: String
)