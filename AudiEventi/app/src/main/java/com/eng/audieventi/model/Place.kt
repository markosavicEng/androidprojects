package com.eng.audieventi.model

data class Place(
    val id: String,
    val title: String,
    val description: String,
    val placeSubtitle: String,
    val placeTitle: String,
    val placeCarousel: ArrayList<BackgroundImage>,
    val placeImage: BackgroundImage
)