package com.eng.audieventi.model

import com.eng.audieventi.enumeration.CouponStatus
import com.eng.audieventi.enumeration.Result

interface CouponProcedure {
    val coupon: String
    val eventId: String
    val status: CouponStatus
    val valid: Boolean
    val result: Result
    val resultCode: String
    val resultMessage: String
}