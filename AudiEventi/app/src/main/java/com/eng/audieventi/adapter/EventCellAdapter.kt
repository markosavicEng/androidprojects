package com.eng.audieventi.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.CustomViewHolder
import com.eng.audieventi.R
import com.eng.audieventi.model.PublicEventAndImage

class EventCellAdapter(val publicEventsAndImagesList: ArrayList<PublicEventAndImage>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cell = layoutInflater.inflate(R.layout.event_cell, parent, false)
        return CustomViewHolder(cell)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val titleTextView = holder.view.findViewById<TextView>(R.id.eventCellTitleTextView)
        val descriptionTextView = holder.view.findViewById<TextView>(R.id.eventCellDescriptionTextView)
        val imageView = holder.view.findViewById<ImageView>(R.id.eventCellImageView)

        titleTextView.text = publicEventsAndImagesList[position].publicEvent.title
        descriptionTextView.text = publicEventsAndImagesList[position].publicEvent.description.value
        imageView.setImageBitmap(publicEventsAndImagesList[position].image)
    }

    override fun getItemCount(): Int {
        return publicEventsAndImagesList.count()
    }

}