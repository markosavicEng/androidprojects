package com.eng.audieventi

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import com.eng.audieventi.model.*
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.DataGlobal
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import java.time.LocalDate

class TestData {

    var image: Bitmap? = null
    val publicEventsModelJson: String =
        """[
            |{ 
            |   'id': '1', 
            |   'titolo': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea'
            |   }, 
            |   'data_evento': '2021-02-27', 
            |   'link_myaudi': { 
            |       'uri': '', 
            |       'title': '' , 
            |       'options': []
            |   }, 
            |   'priorita': 1, 
            |   'stato': 'active', 
            |   'immagine_background': { 
            |       'id': '1', 
            |       'href': 'https://cdn.motor1.com/images/mgl/ogjQ4/s1/audi-r8-v10-plus-with-performance-parts.jpg', 
            |       'meta': { 
            |           'alt': '', 
            |           'title': '', 
            |           'width': 1170, 
            |           'height': 507
            |       } 
            |   } 
            },
            |{ 
            |   'id': '2', 
            |   'titolo': 'Cortina e-portrait', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea'
            |   }, 
            |   'data_evento': '2021-02-27', 
            |   'link_myaudi': { 
            |       'uri': '', 
            |       'title': '' , 
            |       'options': []
            |   }, 
            |   'priorita': 1, 
            |   'stato': 'active', 
            |   'immagine_background': { 
            |       'id': '1', 
            |       'href': 'https://secureservercdn.net/160.153.138.71/n5o.7a0.myftpupload.com/wp-content/uploads/2020/10/2020-audi-r8-performance-review.jpg', 
            |       'meta': { 
            |           'alt': '', 
            |           'title': '', 
            |           'width': 1170, 
            |           'height': 507
            |       } 
            |   } 
            },
            |{ 
            |   'id': '3', 
            |   'titolo': 'Performance Workshop con Kristian Ghedina', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea'
            |   }, 
            |   'data_evento': '2021-02-27', 
            |   'link_myaudi': { 
            |       'uri': '', 
            |       'title': '' , 
            |       'options': []
            |   }, 
            |   'priorita': 1, 
            |   'stato': 'active', 
            |   'immagine_background': { 
            |       'id': '1', 
            |       'href': 'https://ag-spots-2020.o.auroraobjects.eu/2020/12/20/other/2880-1800-crop-audi-r8-v10-performance-2019-c294320122020164041_1.jpg', 
            |       'meta': { 
            |           'alt': '', 
            |           'title': '', 
            |           'width': 1170, 
            |           'height': 507
            |       } 
            |   } 
            }
          ]""".trimMargin()

    val premiumEventsModelJson: String =
        """[
            |{ 
            |   'id': '1', 
            |   'titolo': 'Reconnect to nature', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea',
            |       'processed': ''
            |   }, 
            |   'header_premium': 'Reconnect to nature', 
            |   'link_myaudi_premium': [
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       },
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       }
            |   ], 
            |   'note_programma': {
            |       'value': '',
            |       'format': '',
            |       'processed': ''
            |   }, 
            |   'programma_dettagliato': [
            |       {
            |           'giorno': '2021-01-16T01:00:00+01:00',
            |           'activities': [
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               },
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               }
            |           ],
            |           'sottotitolo': 'con Herve Barmasse',
            |           'immagine': {
            |               'id': '1',
            |               'href': 'https://assets3.thrillist.com/v1/image/2923145/1200x630/flatten;crop_down;jpeg_quality=70',
            |               'meta': {
            |                   'alt': '',
            |                   'title': '',
            |                   'width': 1170,
            |                   'height': 507
            |               }
            |           }
            |       }
            |   ]
            },
            |{ 
            |   'id': '2', 
            |   'titolo': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea',
            |       'processed': ''
            |   }, 
            |   'header_premium': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'link_myaudi_premium': [
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       },
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       }
            |   ], 
            |   'note_programma': {
            |       'value': '',
            |       'format': '',
            |       'processed': ''
            |   }, 
            |   'programma_dettagliato': [
            |       {
            |           'giorno': '2021-01-16T01:00:00+01:00',
            |           'activities': [
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               },
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               }
            |           ],
            |           'sottotitolo': '',
            |           'immagine': {
            |               'id': '2',
            |               'href': 'https://secureservercdn.net/160.153.138.71/n5o.7a0.myftpupload.com/wp-content/uploads/2020/10/2020-audi-r8-performance-review.jpg',
            |               'meta': {
            |                   'alt': '',
            |                   'title': '',
            |                   'width': 1170,
            |                   'height': 507
            |               }
            |           }
            |       }
            |   ]
            },
            |{ 
            |   'id': '3', 
            |   'titolo': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'descrizione': { 
            |       'format': 'testo_formattato', 
            |       'value': 'Otto puntate con ospiti d\'eccezione per condividere visioni all\'avanguardia e discutere di una nuova, straordinaria idea',
            |       'processed': ''
            |   }, 
            |   'header_premium': 'Audi Talks Performance: ieri, oggi e domani.', 
            |   'link_myaudi_premium': [
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       },
            |       { 
            |           'uri': '', 
            |           'title': '', 
            |           'options': []
            |       }
            |   ], 
            |   'note_programma': {
            |       'value': '',
            |       'format': '',
            |       'processed': ''
            |   }, 
            |   'programma_dettagliato': [
            |       {
            |           'giorno': '2021-01-16T01:00:00+01:00',
            |           'activities': [
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               },
            |               {
            |                   'inizio': '',
            |                   'fine': '',
            |                   'activity': ''
            |               }
            |           ],
            |           'sottotitolo': '',
            |           'immagine': {
            |               'id': '3',
            |               'href': 'https://ag-spots-2020.o.auroraobjects.eu/2020/12/20/other/2880-1800-crop-audi-r8-v10-performance-2019-c294320122020164041_1.jpg',
            |               'meta': {
            |                   'alt': '',
            |                   'title': '',
            |                   'width': 1170,
            |                   'height': 507
            |               }
            |           }
            |       }
            |   ]
            }
          ]""".trimMargin()

    val foodExperienceModelJson: String =
        """{
            'id': '1',
            'title': 'Scopri le proposte gastronomiche a te riservate.',
            'header': 'Scopri le proposte gastronomiche a te riservate.',
            'sottotitolo_food': {
                'value': '',
                'format': '',
                'processed': ''
            },
            'immagine_food': {
                'id': '1',
                'href': 'https://images1.westend61.de/0000813725pw/wild-berries-in-bowl-LVF06444.jpg',
                'meta': {
                    'alt': '',
                    'title': '',
                    'width': 1170,
                    'height': 507
                }
            },
            'programma_experience': [
                {
                    'giorno': '2021-02-28T01:00:00+01:00',
                    'inizio': 'Dalle 07:00',
                    'tipo': 'Colazione',
                    'attivita': 'Colazione in Hotel',
                    'sito': 'Hotel',
                    'descrizione': "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    'vivande': '',
                    'allergeni': ''
                }
            ]
        }""".trimIndent()

    val eventContactModelJson: String =
        """{
            |'id': '1',
            |'title': 'Contacts title',
            |'testo_libero': {
            |   'value': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            |   'format': ''
            |},
            |immagine_contatti: {
                'id': '1',
                'href': 'https://cf.bstatic.com/xdata/images/hotel/max1024x768/15821335.jpg?k=9aca0b59698fd0aed281b9bf23f9a9d0dc4b0323a550f47a93325f060dcd982d&o=&hp=1',
                'meta': {
                    'alt': '',
                    'title': '',
                    'width': 1170,
                    'height': 507
                }
            }
        |}""".trimMargin()

    val audiDriveExperienceModelJson: String =
        """{
            |'id': '1',
            |'testo_libero': {
            |   'value': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            |   'format': ''
            |},
            |'sottotitolo_ade': '',
            |'titolo_ade': '',
            |'carosello_ade': [
            |   {
            |       'id': '1',
                    'href': 'https://www.altabadia.org/feratel/event/audi-driving-experience-in-alta-badia-corvara_10.jpg',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   },
            |   {
            |       'id': '1',
                    'href': 'https://cdn.motor1.com/images/mgl/ogjQ4/s1/audi-r8-v10-plus-with-performance-parts.jpg',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   },
            |   {
            |       'id': '1',
                    'href': 'https://cdn.motor1.com/images/mgl/ogjQ4/s1/audi-r8-v10-plus-with-performance-parts.jpg',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   }
            |],
            |immagine_ade: {
                'id': '1',
                'href': 'https://www.altabadia.org/feratel/event/audi-driving-experience-in-alta-badia-corvara_10.jpg',
                'meta': {
                    'alt': '',
                    'title': '',
                    'width': 1170,
                    'height': 507
                }
            }
        |}""".trimMargin()

    val placeModelJson: String =
        """{
            |'id': '1',
            |'title': 'Place title',
            |'descrizione': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            |'sottotitolo_luoghi': '',
            |'titolo_luoghi': '',
            |'carosello_luoghi': [
            |   {
            |       'id': '1',
                    'href': 'https://www.pymnts.com/wp-content/uploads/2020/12/france-ski-resort.jpg',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   },
            |   {
            |       'id': '1',
                    'href': 'https://thebarentsobserver.com/sites/default/files/skjermbilde_2020-04-08_kl._09.13.16.png',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   },
            |   {
            |       'id': '1',
                    'href': 'https://www.colorado.com/sites/default/files/AspenSnowmassPRGondola.jpg',
                    'meta': {
                        'alt': '',
                        'title': '',
                        'width': 1170,
                        'height': 507
                    }
            |   }
            |],
            |immagine_luoghi: {
                'id': '1',
                'href': 'https://www.pymnts.com/wp-content/uploads/2020/12/france-ski-resort.jpg',
                'meta': {
                    'alt': '',
                    'title': '',
                    'width': 1170,
                    'height': 507
                }
            }
        |}""".trimMargin()


    fun loadPublicEventJsonData() {
        DataGlobal.publicEventsList.clear()
        DataGlobal.publicEventsAndImagesList.clear()

        val jsonArray = JSONArray(publicEventsModelJson)

        jsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = jsonArray.getJSONObject(it)

                val id = jsonObject.getString("id")
                val title = jsonObject.getString("titolo")

                val description = jsonObject.getJSONObject("descrizione")
                val value = description.getString("value")
                val format = description.getString("format")

                val eventDate = jsonObject.getString("data_evento")

                val linkMyAudi = jsonObject.getJSONObject("link_myaudi")
                val uri = linkMyAudi.getString("uri")
                val linkMyAudiTitle = linkMyAudi.getString("title")
                val options = linkMyAudi.getJSONArray("options")
                val optionsArrayList: ArrayList<Option> = arrayListOf()
                /* TODO Insert options into array */
                options.let {
                    (0 until it.length()).forEach {
//                        optionsArrayList.add(jsonArray.getJSONObject(it))
                    }
                }

                val priority = jsonObject.getInt("priorita")
                val status = jsonObject.getString("stato")

                val backgroundImage = jsonObject.getJSONObject("immagine_background")
                val backgroundImageId = backgroundImage.getString("id")
                val href = backgroundImage.getString("href")
                val meta = backgroundImage.getJSONObject("meta")
                val metaAlt = meta.getString("alt")
                val metaTitle = meta.getString("title")
                val metaWidth = meta.getInt("width")
                val metaHeight = meta.getInt("height")

                val publicEvent = PublicEvent(
                    id,
                    title,
                    PublicEventDescription(
                        value,
                        format
                    ),
                    LocalDate.parse(eventDate),
                    LinkMyAudi(
                        uri,
                        linkMyAudiTitle,
                        optionsArrayList
                    ),
                    priority,
                    status,
                    BackgroundImage(
                        backgroundImageId,
                        href,
                        Meta(
                            metaAlt,
                            metaTitle,
                            metaWidth,
                            metaHeight
                        )
                    )
                )

                // TODO consider removing publicEventsList in favor of publicEventsAndImagesList
                DataGlobal.publicEventsList.add(publicEvent)

                println("*** LOAD IMAGE ***")
                ApiJsonData.loadImageFromUrl(URL(publicEvent.backgroundImage.href)) {
                    // function passed as parameter
                    addPublicEventAndImageToArray(publicEvent)
                    DataGlobal.publicEventsAndImagesList.sortedWith(compareBy { it.publicEvent.id.toInt() })
                }
            }
        }
    }

    fun loadPremiumEventJsonData() {
        val jsonArray = JSONArray(premiumEventsModelJson)

        val premiumEventBackgroundImagesUrlList: ArrayList<URL> = arrayListOf()

        DataGlobal.premiumEventsList.clear()
        DataGlobal.premiumEventsAndImagesList.clear()

        println("****** loadPremiumEventJsonData ******")
        jsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = jsonArray.getJSONObject(it)

                val id = jsonObject.getString("id")
                val title = jsonObject.getString("titolo")

                val description = jsonObject.getJSONObject("descrizione")
                val value = description.getString("value")
                val format = description.getString("format")
                val processed = description.getString("processed")
                val descriptionObject = PremiumEventDescription(value, format, processed)

                val headerPremium = jsonObject.getString("header_premium")

                val linkMyAudiPremium = jsonObject.getJSONArray("link_myaudi_premium")
                val linkMyAudiPremiumArrayList: ArrayList<LinkMyAudi> = arrayListOf()
                linkMyAudiPremium.let {
                    (0 until it.length()).forEach {
                        val linkMyAudi = linkMyAudiPremium.getJSONObject(it)
                        val uri = linkMyAudi.getString("uri")
                        val linkMyAudiTitle = linkMyAudi.getString("title")
                        val options = linkMyAudi.getJSONArray("options")

                        val optionsArrayList: ArrayList<Option> = arrayListOf()
                        /* TODO Insert options into array */
                        options.let {
                            (0 until it.length()).forEach {
//                              optionsArrayList.add(Option())
                            }
                        }
                        linkMyAudiPremiumArrayList.add(
                            LinkMyAudi(
                            uri,
                            linkMyAudiTitle,
                            optionsArrayList
                        ))
                    }
                }

                val programNotes = jsonObject.getJSONObject("note_programma")
                val programNotesValue = programNotes.getString("value")
                val programNotesFormat = programNotes.getString("format")
                val programNotesProcessed = programNotes.getString("processed")
                val programNotesObject = ProgramNotes(
                    programNotesValue,
                    programNotesFormat,
                    programNotesProcessed)

                val detailedProgram = jsonObject.getJSONArray("programma_dettagliato")
                val detailedProgramArrayList: ArrayList<EventDay> = arrayListOf()
                detailedProgram.let {
                    (0 until it.length()).forEach {
                        val eventDay = detailedProgram.getJSONObject(it)
                        val day = eventDay.getString("giorno")
                        val activities = eventDay.getJSONArray("activities")
                        val activitiesArrayList: ArrayList<DailyActivity> = arrayListOf()
                        activities.let {
                            (0 until it.length()).forEach {
                                val dailyActivity = activities.getJSONObject(it)
                                val start = dailyActivity.getString("inizio")
                                val end = dailyActivity.getString("fine")
                                val activity = dailyActivity.getString("activity")
                                activitiesArrayList.add(DailyActivity(start, end, activity))
                            }
                        }
                        val subtitle = eventDay.getString("sottotitolo")
                        val image = eventDay.getJSONObject("immagine")
                        val imageId = image.getString("id")
                        val imageHref = image.getString("href")

                        val imageMeta = image.getJSONObject("meta")
                        val imageMetaAlt = imageMeta.getString("alt")
                        val imageMetaTitle = imageMeta.getString("title")
                        val imageMetaWidth = imageMeta.getInt("width")
                        val imageMetaHeight = imageMeta.getInt("height")
                        val meta = Meta(imageMetaAlt, imageMetaTitle, imageMetaWidth, imageMetaHeight)

                        val imageObject = BackgroundImage(imageId, imageHref, meta)

                        val eventDayObject = EventDay(day, activitiesArrayList, subtitle, imageObject)
                        detailedProgramArrayList.add(eventDayObject)

                        premiumEventBackgroundImagesUrlList.add(URL(imageHref))
                    }
                }

                val premiumEvent = PremiumEvent(
                    id,
                    title,
                    descriptionObject,
                    headerPremium,
                    linkMyAudiPremiumArrayList,
                    programNotesObject,
                    detailedProgramArrayList
                )

                DataGlobal.premiumEventsList.add(premiumEvent)

                ApiJsonData.loadImageListFromUrlList(premiumEventBackgroundImagesUrlList) {
                    addPremiumEventAndImageToArray(premiumEvent, ApiJsonData.loadedImageList)
                    DataGlobal.premiumEventsAndImagesList.sortedWith(compareBy { it.premiumEvent.id.toInt() })
                }

                println(premiumEvent)
            }
        }
        println("****** ****** ****** ******")
    }

    fun loadFoodExperienceJsonData() {
        val jsonObject = JSONObject(foodExperienceModelJson)
        println("****** loadFoodExperienceJsonData ******")

        val id = jsonObject.getString("id")
        val title = jsonObject.getString("title")
        val header = jsonObject.getString("header")
        val foodSubtitle = jsonObject.getJSONObject("sottotitolo_food")
        val format = foodSubtitle.getString("format")
        val value = foodSubtitle.getString("value")
        val processed = foodSubtitle.getString("processed")
        val foodSubtitleObject = FoodSubtitle(value, format, processed)

        val foodImage = jsonObject.getJSONObject("immagine_food")

        val backgroundImageId = foodImage.getString("id")
        val href = foodImage.getString("href")
        val meta = foodImage.getJSONObject("meta")
        val metaAlt = meta.getString("alt")
        val metaTitle = meta.getString("title")
        val metaWidth = meta.getInt("width")
        val metaHeight = meta.getInt("height")

        val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
        val foodImageObject = BackgroundImage(backgroundImageId, href, metaObject)

        val experienceProgram = jsonObject.getJSONArray("programma_experience")
        val experienceProgramArrayList: ArrayList<DailyFoodExperience> = arrayListOf()
        experienceProgram.let {
            (0 until it.length()).forEach {
                val dailyFoodExperience = experienceProgram.getJSONObject(it)
                val day = dailyFoodExperience.getString("giorno")
                val start = dailyFoodExperience.getString("inizio")
                val type = dailyFoodExperience.getString("tipo")
                val activity = dailyFoodExperience.getString("attivita")
                val site = dailyFoodExperience.getString("sito")
                val description = dailyFoodExperience.getString("descrizione")
                val food = dailyFoodExperience.getString("vivande")
                val allergens = dailyFoodExperience.getString("allergeni")

                val dailyFoodExperienceObject = DailyFoodExperience(day, start, type, activity, site, description, food, allergens)
                experienceProgramArrayList.add(dailyFoodExperienceObject)
            }
        }

        val foodExperience = FoodExperience(
            id,
            title,
            header,
            foodSubtitleObject,
            foodImageObject,
            experienceProgramArrayList
        )

        ApiJsonData.loadImageFromUrl(URL(foodExperience.foodImage.href)) {
            DataGlobal.foodExperienceAndImage = FoodExperienceAndImage(foodExperience, ApiJsonData.loadedImage)
        }

        println(foodExperience)
        println("****** ****** ****** ******")
    }

    fun loadEventContactJsonData() {
        val jsonObject = JSONObject(eventContactModelJson)
        println("****** loadEventContactJsonData ******")

        val id = jsonObject.getString("id")
        val title = jsonObject.getString("title")

        val freeText = jsonObject.getJSONObject("testo_libero")
        val value = freeText.getString("value")
        val format = freeText.getString("format")
        val freeTextObject = FreeText(value, format)

        val contactImage = jsonObject.getJSONObject("immagine_contatti")
        val contactImageId = contactImage.getString("id")
        val href = contactImage.getString("href")
        val meta = contactImage.getJSONObject("meta")
        val metaAlt = meta.getString("alt")
        val metaTitle = meta.getString("title")
        val metaWidth = meta.getInt("width")
        val metaHeight = meta.getInt("height")
        val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
        val contactImageObject = BackgroundImage(contactImageId, href, metaObject)

        val eventContact = EventContact(
            id,
            title,
            freeTextObject,
            contactImageObject
        )

        ApiJsonData.loadImageFromUrl(URL(eventContact.contactImage.href)) {
            DataGlobal.eventContactAndImage = EventContactAndImage(eventContact, ApiJsonData.loadedImage)
        }

        println(eventContact)
        println("****** ****** ****** ******")
    }

    fun loadAudiDriveExperienceJsonData() {
        val jsonObject = JSONObject(audiDriveExperienceModelJson)

        val adeCarouselUrlList: ArrayList<URL> = arrayListOf()

        println("****** loadAudiDriveExperience ******")

        val id = jsonObject.getString("id")

        val freeText = jsonObject.getJSONObject("testo_libero")
        val value = freeText.getString("value")
        val format = freeText.getString("format")
        val freeTextObject = FreeText(value, format)

        val adeSubtitle = jsonObject.getString("sottotitolo_ade")
        val adeTitle = jsonObject.getString("titolo_ade")

        val adeCarousel = jsonObject.getJSONArray("carosello_ade")
        val adeCarouselArrayList: ArrayList<BackgroundImage> = arrayListOf()
        adeCarousel.let {
            (0 until it.length()).forEach {
                val image = adeCarousel.getJSONObject(it)
                val imageId = image.getString("id")
                val href = image.getString("href")
                val meta = image.getJSONObject("meta")
                val metaAlt = meta.getString("alt")
                val metaTitle = meta.getString("title")
                val metaWidth = meta.getInt("width")
                val metaHeight = meta.getInt("height")
                val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
                val imageObject = BackgroundImage(imageId, href, metaObject)

                adeCarouselArrayList.add(imageObject)

                adeCarouselUrlList.add(URL(href))
            }
        }

        val adeImage = jsonObject.getJSONObject("immagine_ade")
        val adeImageId = adeImage.getString("id")
        val href = adeImage.getString("href")
        val meta = adeImage.getJSONObject("meta")
        val metaAlt = meta.getString("alt")
        val metaTitle = meta.getString("title")
        val metaWidth = meta.getInt("width")
        val metaHeight = meta.getInt("height")
        val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
        val adeImageObject = BackgroundImage(adeImageId, href, metaObject)

        val ade = AudiDriveExperience(
            id,
            freeTextObject,
            adeSubtitle,
            adeTitle,
            adeCarouselArrayList,
            adeImageObject
        )

        ApiJsonData.loadImageFromUrl(URL(ade.adeImage.href)) {
            ApiJsonData.loadImageListFromUrlList(adeCarouselUrlList) {
                DataGlobal.audiDriveExperienceAndImageAndCarousel = AudiDriveExperienceAndImageAndCarousel(ade, ApiJsonData.loadedImage, ApiJsonData.loadedImageList)
            }
        }

        println(ade)
        println("****** ****** ****** ******")
    }

    fun loadPlaceJsonData() {
        val jsonObject = JSONObject(placeModelJson)

        val placeCarouselUrlList: ArrayList<URL> = arrayListOf()

        println("****** loadPlaceJsonData ******")

        val id = jsonObject.getString("id")
        val title = jsonObject.getString("title")
        val description = jsonObject.getString("descrizione")


        val placeSubtitle = jsonObject.getString("sottotitolo_luoghi")
        val placeTitle = jsonObject.getString("titolo_luoghi")

        val placeCarousel = jsonObject.getJSONArray("carosello_luoghi")
        val placeCarouselArrayList: ArrayList<BackgroundImage> = arrayListOf()
        placeCarousel.let {
            (0 until it.length()).forEach {
                val image = placeCarousel.getJSONObject(it)
                val imageId = image.getString("id")
                val href = image.getString("href")
                val meta = image.getJSONObject("meta")
                val metaAlt = meta.getString("alt")
                val metaTitle = meta.getString("title")
                val metaWidth = meta.getInt("width")
                val metaHeight = meta.getInt("height")
                val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
                val imageObject = BackgroundImage(imageId, href, metaObject)

                placeCarouselArrayList.add(imageObject)

                placeCarouselUrlList.add(URL(href))
            }
        }

        val placeImage = jsonObject.getJSONObject("immagine_luoghi")
        val placeImageId = placeImage.getString("id")
        val href = placeImage.getString("href")
        val meta = placeImage.getJSONObject("meta")
        val metaAlt = meta.getString("alt")
        val metaTitle = meta.getString("title")
        val metaWidth = meta.getInt("width")
        val metaHeight = meta.getInt("height")
        val metaObject = Meta(metaAlt, metaTitle, metaWidth, metaHeight)
        val placeImageObject = BackgroundImage(placeImageId, href, metaObject)

        val place = Place(
            id,
            title,
            description,
            placeSubtitle,
            placeTitle,
            placeCarouselArrayList,
            placeImageObject
        )

        ApiJsonData.loadImageFromUrl(URL(place.placeImage.href)) {
            ApiJsonData.loadImageListFromUrlList(placeCarouselUrlList) {
                DataGlobal.placeAndImageAndCarousel = PlaceAndImageAndCarousel(place, ApiJsonData.loadedImage, ApiJsonData.loadedImageList)
            }
        }

        println(place)
        println("****** ****** ****** ******")
    }

    inner class loadImageFromURL : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val url = params[0]

            try {
                val imageFromJson = URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromJson)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            image = result
        }
    }

    fun addPublicEventAndImageToArray(publicEvent: PublicEvent) {
        DataGlobal.publicEventsAndImagesList.add(
            PublicEventAndImage(
                publicEvent,
                ApiJsonData.loadedImage
            )
        )
    }

    fun addPremiumEventAndImageToArray(premiumEvent: PremiumEvent, imageList: ArrayList<Bitmap>?) {
        DataGlobal.premiumEventsAndImagesList.add(
            PremiumEventAndImage(
                premiumEvent,
                imageList
            )
        )
    }
}