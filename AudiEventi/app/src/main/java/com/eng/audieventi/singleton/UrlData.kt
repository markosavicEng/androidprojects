package com.eng.audieventi.singleton

object UrlData {
    const val audiEventiRootUrl = "http://mobileapp-coll.engds.it/AudiEventi"
    const val langsPath = "/langs/it_IT.json"

    fun getLangsUrl() : String {
        return audiEventiRootUrl + langsPath
    }
}