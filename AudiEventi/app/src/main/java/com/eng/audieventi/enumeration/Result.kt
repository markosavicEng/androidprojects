package com.eng.audieventi.enumeration

enum class Result (val value: String) {
    OK("OK"),
    KO("KO")
}