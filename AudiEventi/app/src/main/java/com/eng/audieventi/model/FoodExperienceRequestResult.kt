package com.eng.audieventi.model

import com.eng.audieventi.enumeration.Result

class FoodExperienceRequestResult(
    override val result: Result,
    override val resultCode: String,
    override val resultMessage: String,

    val data: FoodExperience
) : RequestResult