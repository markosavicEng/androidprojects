package com.eng.audieventi.model

interface EventDescription {
    val value: String
    val format: String
}