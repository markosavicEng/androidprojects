package com.eng.audieventi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eng.audieventi.adapter.EventCellAdapter
import com.eng.audieventi.R
import com.eng.audieventi.model.PublicEventAndImage
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.DataGlobal

class CalendarioEventiFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_calendario_eventi, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews(view)
    }

    private fun setViews(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.calendarioEventiRecyclerView)
        recyclerView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)

        recyclerView.adapter = EventCellAdapter(DataGlobal.publicEventsAndImagesList)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }
}