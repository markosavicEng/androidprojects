package com.eng.audieventi.model

data class AudiDriveExperience(
    val id: String,
    /* TODO Check if freeText is the correct data type */
    val freeText: FreeText,
    /* TODO Check if ADESubtitle and ADETitle are the correct data types */
    val adeSubtitle: String,
    val adeTitle: String,
    val adeCarousel: ArrayList<BackgroundImage>,
    val adeImage: BackgroundImage
)