package com.eng.audieventi.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.eng.audieventi.R
import com.eng.audieventi.activity.MainActivity
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.DataGlobal

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews(view)
    }

    /* ------------- VIEW SETTER FUNCTIONS ------------- */

    private fun setViews(view: View) {
        val mainActivity = (activity as MainActivity)

        val homePremiumContentLayout = view.findViewById<ConstraintLayout>(R.id.homePremiumContentLayout)

            setPublicContentViews(view, mainActivity)

        // TODO optimize the waiting time for the Bitmap to be loaded
        if (DataGlobal.isPremiumUser) {
                setPremiumContentViews(view, mainActivity)
                homePremiumContentLayout.visibility = View.VISIBLE
        }

    }

    private fun setPremiumContentViews(view: View, mainActivity: MainActivity) {
        setPremiumEventViews(view, mainActivity)
        setFoodExperienceViews(view, mainActivity)
        setLuoghiETerritorioViews(view, mainActivity)
        setAudiDriveExperienceViews(view, mainActivity)
        setinfoAndContattiViews(view, mainActivity)
        setSondaggioViews(view, mainActivity)
    }

    fun setPublicContentViews(view: View, mainActivity: MainActivity) {
        setEventiAudiViews(view, mainActivity)
        setIlTuoEventoViews(view, mainActivity)
        setGammaAudiViews(view, mainActivity)
    }

    private fun setPremiumEventViews(view: View, mainActivity: MainActivity) {
        val premiumEventImageView = view.findViewById<ImageView>(R.id.premiumEventImageView)
        val premiumEventTitleTextView = view.findViewById<TextView>(R.id.premiumEventTitleTextView)
        val premiumEventAdditionalTextView = view.findViewById<TextView>(R.id.premiumEventAdditionalTextView)

        premiumEventImageView.setImageBitmap(DataGlobal.premiumEventsAndImagesList[0].imageList?.get(0))
        premiumEventTitleTextView.text = DataGlobal.premiumEventsAndImagesList[0].premiumEvent.title
        premiumEventAdditionalTextView.text = DataGlobal.premiumEventsAndImagesList[0].premiumEvent.detailedProgram[0].subtitle
    }

    private fun setFoodExperienceViews(view: View, mainActivity: MainActivity) {
        val foodExperienceImageView = view.findViewById<ImageView>(R.id.foodExperienceImageView)
        val foodExperienceTitleTextView = view.findViewById<TextView>(R.id.foodExperienceTitleTextView)

        foodExperienceTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_FOOD_EXPERIENCE_TITLE"]
        foodExperienceImageView.setImageBitmap(DataGlobal.foodExperienceAndImage?.image)
    }

    private fun setLuoghiETerritorioViews(view: View, mainActivity: MainActivity) {
        val luoghiETerritorioImageView = view.findViewById<ImageView>(R.id.luoghiETerritorioImageView)
        val luoghiETerritorioTitleTextView = view.findViewById<TextView>(R.id.luoghiETerritorioTitleTextView)
        val luoghiETerritorioAdditionalTextView = view.findViewById<TextView>(R.id.luoghiETerritorioAdditionalTextView)

        luoghiETerritorioImageView.setImageBitmap(DataGlobal.placeAndImageAndCarousel?.image)
        luoghiETerritorioTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_PLACES_TITLE"]
        luoghiETerritorioAdditionalTextView.text = ApiJsonData.langsJsonDataMap["HOME_PLACES_SUBTITLE"]
    }

    private fun setAudiDriveExperienceViews(view: View, mainActivity: MainActivity) {
        val audiDriveExperienceImageView = view.findViewById<ImageView>(R.id.audiDriveExperienceImageView)
        val audiDriveExperienceTitleTextView = view.findViewById<TextView>(R.id.audiDriveExperienceTitleTextView)

        audiDriveExperienceImageView.setImageBitmap(DataGlobal.audiDriveExperienceAndImageAndCarousel?.image)
        audiDriveExperienceTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_ADE_SUBTITLE"]
    }

    private fun setinfoAndContattiViews(view: View, mainActivity: MainActivity) {
        val infoAndContattiImageView = view.findViewById<ImageView>(R.id.infoAndContattiImageView)

        infoAndContattiImageView.setImageBitmap(DataGlobal.eventContactAndImage?.image)
    }

    private fun setSondaggioViews(view: View, mainActivity: MainActivity) {
        val sondaggioTitleTextView = view.findViewById<TextView>(R.id.sondaggioTitleTextView)
        val sondaggioAdditionalTextView = view.findViewById<TextView>(R.id.sondaggioAdditionalTextView)
        val sondaggioImageView = view.findViewById<ImageView>(R.id.sondaggioImageView)

        sondaggioTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_SURVEY_TITLE"]
        sondaggioAdditionalTextView.text = ApiJsonData.langsJsonDataMap["SURVEY_BOTTOM_HEADER"]
        sondaggioImageView.setImageResource(R.drawable.sondaggio)
    }

    private fun setEventiAudiViews(view: View, mainActivity: MainActivity) {
        val eventiAudiLayout: ConstraintLayout = view.findViewById(R.id.eventiAudiLayout)
        val eventiAudiTitleTextView = view.findViewById<TextView>(R.id.eventiAudiTitleTextView)
        val eventiAudiAdditionalTextView = view.findViewById<TextView>(R.id.eventiAudiAdditionalTextView)

        eventiAudiTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_GUEST_EVENT_LIST"]
        eventiAudiAdditionalTextView.text = ApiJsonData.langsJsonDataMap["HOME_GUEST_EVENT_LIST_BUTTON"]

        eventiAudiLayout.setOnClickListener {
            mainActivity.startCalendariEventioPage()
        }
    }

    private fun setIlTuoEventoViews(view: View, mainActivity: MainActivity) {
        val ilTuoEventoLayout: ConstraintLayout = view.findViewById(R.id.ilTuoEventoLayout)
        val ilTuoEventoTitleTextView = view.findViewById<TextView>(R.id.ilTuoEventoTitleTextView)

        ilTuoEventoTitleTextView.text = ApiJsonData.langsJsonDataMap["PROGRAM_APPBAR_TITLE"]

        ilTuoEventoLayout.setOnClickListener {
            mainActivity.startCouponEventiDialog()
        }
    }

    fun setGammaAudiViews(view: View, mainActivity: MainActivity) {
        val gammaAudiTitleTextView = view.findViewById<TextView>(R.id.gammaAudiTitleTextView)
        val gammaAudiAdditionalTextView = view.findViewById<TextView>(R.id.gammaAudiAdditionalTextView)

        gammaAudiTitleTextView.text = ApiJsonData.langsJsonDataMap["HOME_GUEST_AUDI_RANGE"]
        gammaAudiAdditionalTextView.text = ApiJsonData.langsJsonDataMap["HOME_GUEST_COUPON_LOGIN"]
    }
}