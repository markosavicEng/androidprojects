package com.eng.audieventi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.eng.audieventi.R
import com.eng.audieventi.fragment.HomeFragment
import com.eng.audieventi.singleton.ApiJsonData

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        ApiJsonData.loadJsonDataFromApi()

        // TODO try to find a better way to delay the splash screen
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
        }, 2000)
    }
}