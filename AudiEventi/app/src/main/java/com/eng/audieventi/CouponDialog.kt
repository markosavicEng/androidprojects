package com.eng.audieventi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.eng.audieventi.enumeration.DialogButtonType

class CouponDialog(private val instructions: String?,
                   private val proceedButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>?,
                   private val closeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>?
)
    : DialogFragment() {

        class Builder {
            var instructions: String? = null

            var proceedButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>? = null
            private set

            var closeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType>? = null
            private set

            fun setProceedButton(text: String, listener: View.OnClickListener, buttonType: DialogButtonType = DialogButtonType.PROCEED) {
                proceedButtonTriple = Triple(text, listener, buttonType)
            }

            fun setCloseButton(listener: View.OnClickListener, buttonType: DialogButtonType = DialogButtonType.CLOSE) {
                closeButtonTriple = Triple("", listener, buttonType)
            }

            fun build() = CouponDialog(this)
        }

    private constructor(builder: Builder) : this (
        builder.instructions,
        builder.proceedButtonTriple,
        builder.closeButtonTriple
    )

    companion object {
        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.coupon_dialog, container, false)

    override fun onStart() {
        super.onStart()
        dialog?.let { dialog ->
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT

            dialog.window?.let {
                it.apply {
                    setLayout(width, height)
                }
            }
        }
    }

    private fun Button.setupAsDialogButton(buttonText: String?, clickListener: View.OnClickListener?) {
        if (buttonText != null && clickListener != null) {
            this.apply {
                text = buttonText
                visibility = View.VISIBLE
                setOnClickListener {
                    clickListener.onClick(this)
                    dismiss()
                }
            }
        }
    }

    private fun ImageView.setupAsDialogButton(clickListener: View.OnClickListener?) {
        if (clickListener != null) {
            this.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    clickListener.onClick(this)
                    dismiss()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val instructionsTextView = view.findViewById<TextView>(R.id.couponDialogInstructionsTextView)
        instructionsTextView.text = instructions

        val proceedButton = view.findViewById<Button>(R.id.couponDialogButton)
        val closeImageView = view.findViewById<ImageView>(R.id.couponDialogCloseImageView)

        proceedButtonTriple.let {
            if (proceedButtonTriple != null) {
                proceedButton.setupAsDialogButton(proceedButtonTriple.first, proceedButtonTriple.second)
//                proceedButton.setTypeStyle(proceedButtonTriple.third)
            }
        }

        closeButtonTriple.let {
            if (closeButtonTriple != null) {
                closeImageView.setupAsDialogButton(closeButtonTriple.second)
//                negativeButton.setTypeStyle(negativeButtonTriple.third)
            }
        }
    }
}