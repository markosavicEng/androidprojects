package com.eng.audieventi.model

import android.graphics.Bitmap

class FoodExperienceAndImage(
    val foodExperience: FoodExperience,
    val image: Bitmap?
)