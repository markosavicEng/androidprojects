package com.eng.audieventi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import androidx.drawerlayout.widget.DrawerLayout
import com.eng.audieventi.CouponDialog
import com.eng.audieventi.R
import com.eng.audieventi.TestData
import com.eng.audieventi.enumeration.DialogButtonType
import com.eng.audieventi.fragment.CalendarioEventiFragment
import com.eng.audieventi.fragment.HomeFragment
import com.eng.audieventi.fragment.ImpostazioniFragment
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.DataGlobal
import com.google.android.material.navigation.NavigationView

lateinit var drawerLayout: DrawerLayout
lateinit var navigationView: NavigationView
lateinit var settingsNavigationView: NavigationView

class MainActivity : AppCompatActivity() {

    var dialog: CouponDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawerLayout = findViewById(R.id.homeDrawerLayout)
        navigationView = findViewById(R.id.navigationView)
        settingsNavigationView = findViewById(R.id.settingsNavigationView)

        val hamburgerMenu = findViewById<ImageView>(R.id.hamburgerMenuImageView)

        hamburgerMenu.setOnClickListener {
            openDrawerLayout()
        }

//        ApiJsonData.loadJsonDataFromApi()

        // delay until the langsDataMap is filled with data (after onPostExecute() method)
        Handler(Looper.getMainLooper()).postDelayed({
            println(ApiJsonData.langsJsonDataMap["HOME_GUEST_EVENT_LIST"])
        }, 1000)


        // checking test data
        // TODO move data loading to specific fragments or to splash screen
        val testDataInstance = TestData()

//        Handler(Looper.getMainLooper()).postDelayed({
//            testDataInstance.loadPublicEventJsonData()
//        }, 1000)

        println("*** START LOADING DATA ***")
        testDataInstance.loadPublicEventJsonData()
        testDataInstance.loadPremiumEventJsonData()
        testDataInstance.loadFoodExperienceJsonData()
        testDataInstance.loadEventContactJsonData()
        testDataInstance.loadAudiDriveExperienceJsonData()
        testDataInstance.loadPlaceJsonData()

        setViews()

        // Pause for allowing data to be loaded before HomeFragment starts
        Handler(Looper.getMainLooper()).postDelayed({
            startHomePage()
        }, 2500)
    }

    fun setNavigationMenuItemSelectedListeners() {

        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.homeMenuItem -> startHomePage()
                R.id.calendarioEventiMenuItem -> startCalendariEventioPage()
                R.id.couponEventiMenuItem -> startCouponEventiDialog()
                R.id.impostazioniMenuItem -> startImpostazioniPage()
            }
            true
        }

        settingsNavigationView.setNavigationItemSelectedListener {
            startImpostazioniPage()
            true
        }
    }

    fun startHomePage() {
        val homeFragment = HomeFragment()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrameLayout, homeFragment)
            commit()
        }
        closeDrawerLayout()
    }

    fun startCalendariEventioPage() {
        val calendarioEventiFragment = CalendarioEventiFragment()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrameLayout, calendarioEventiFragment)
            commit()
        }
        closeDrawerLayout()
    }

    fun startCouponEventiDialog() {
        dialog = CouponDialog.build {
            instructions = ApiJsonData.langsJsonDataMap["COUPON_LOGIN_MESSAGE"]

            setProceedButton(ApiJsonData.langsJsonDataMap["COUPON_LOGIN_BUTTON"]!!, View.OnClickListener {
                DataGlobal.isPremiumUser = true
                println("PREMIUM USER: ${DataGlobal.isPremiumUser}")
                dialog?.dismiss()
                restartMainActivity()
            }, DialogButtonType.PROCEED)

            setCloseButton(View.OnClickListener {
                dialog?.dismiss()
            }, DialogButtonType.CLOSE)
        }
        dialog?.show(supportFragmentManager, "TAG")
    }

    fun startImpostazioniPage() {
        val impostazioniFragment = ImpostazioniFragment()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrameLayout, impostazioniFragment)
            commit()
        }
        closeDrawerLayout()
    }

    fun restartMainActivity() {
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }

    /* ---------- DRAWER LAYOUT FUNCTIONS ---------- */
    fun openDrawerLayout() {
        drawerLayout.openDrawer(navigationView)
    }

    fun closeDrawerLayout() {
        drawerLayout.closeDrawer(navigationView)
    }

    /* ---------- VIEW SETTER FUNCTIONS ---------- */
    fun setViews() {

        val notificationBellImageView = findViewById<ImageView>(R.id.notificationBellImageView)

        setNavigationMenuViews()

        if (!DataGlobal.isPremiumUser) {
            notificationBellImageView.visibility = View.GONE
        } else {
            notificationBellImageView.visibility = View.VISIBLE
        }
    }

    fun setNavigationMenuViews() {
        navigationView = findViewById(R.id.navigationView)
        settingsNavigationView = findViewById(R.id.settingsNavigationView)

//        val homeMenuItemGroup = navigationView.menu.findItem(R.id.homeMenuItem)
        val programmaMenuItem = navigationView.menu.findItem(R.id.programmaMenuItem)
        val foodExperienceMenuItem = navigationView.menu.findItem(R.id.foodExperienceMenuItem)
        val luoghiETerrirorioMenuItem = navigationView.menu.findItem(R.id.luoghiETerritorioMenuItem)
        val audiDriveExperienceMenuItem = navigationView.menu.findItem(R.id.audiDriveExperienceMenuItem)
        val sondaggioMenuItem = navigationView.menu.findItem(R.id.sondaggioMenuItem)
//        val calendarioEventiMenuItem = navigationView.menu.findItem(R.id.calendarioEventiMenuItem)
//        val couponEventiMenuItem = navigationView.menu.findItem(R.id.couponEventiMenuItem)
        val infoAndContattiMeniItem = navigationView.menu.findItem(R.id.infoAndContattiMenuItem)
        val impostazioniMenuItem = navigationView.menu.findItem(R.id.impostazioniMenuItem)
//        val impostazioniPublicMenuItem = settingsNavigationView.menu.findItem(R.id.impostazioniPublicMenuItem)

        if (!DataGlobal.isPremiumUser) {
            programmaMenuItem.isVisible = false
            foodExperienceMenuItem.isVisible = false
            luoghiETerrirorioMenuItem.isVisible = false
            audiDriveExperienceMenuItem.isVisible = false
            sondaggioMenuItem.isVisible = false
            infoAndContattiMeniItem.isVisible = false
            impostazioniMenuItem.isVisible = false
            settingsNavigationView.visibility = View.VISIBLE
        } else {
            programmaMenuItem.isVisible = true
            foodExperienceMenuItem.isVisible = true
            luoghiETerrirorioMenuItem.isVisible = true
            audiDriveExperienceMenuItem.isVisible = true
            sondaggioMenuItem.isVisible = true
            infoAndContattiMeniItem.isVisible = true
            impostazioniMenuItem.isVisible = true
            settingsNavigationView.visibility = View.GONE
        }

        setNavigationMenuItemSelectedListeners()
    }
}