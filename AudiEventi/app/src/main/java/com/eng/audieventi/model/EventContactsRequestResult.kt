package com.eng.audieventi.model

import com.eng.audieventi.enumeration.Result

class EventContactsRequestResult(
    override val result: Result,
    override val resultCode: String,
    override val resultMessage: String,

    val data: EventContact
) : RequestResult