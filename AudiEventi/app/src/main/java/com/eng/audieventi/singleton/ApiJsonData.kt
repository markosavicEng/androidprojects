package com.eng.audieventi.singleton

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.eng.audieventi.ApiJsonDataLoader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.URL

object ApiJsonData : AppCompatActivity() {
    var langsJsonDataMap: MutableMap<String, String> = mutableMapOf()
    var loadedImage: Bitmap? = null
    var loadedImageList: ArrayList<Bitmap>? = arrayListOf()

    fun loadJsonDataFromApi() {
        ApiJsonDataLoader(UrlData.getLangsUrl()).execute(UrlData.getLangsUrl())
    }

    fun loadImageFromUrl(url: URL, action: (() -> Unit)? = null) {

        lifecycleScope.launch(Dispatchers.IO) {
            url.getBitmap()?.apply {
                withContext(Dispatchers.Main) {
                    loadedImage = this@apply
                    action!!()
                }
            }
        }
    }

    fun loadImageListFromUrlList(urlList: ArrayList<URL>, action: (() -> Unit)? = null) {

        for (url in urlList) {
            lifecycleScope.launch(Dispatchers.IO) {
                url.getBitmap()?.apply {
                    withContext(Dispatchers.Main) {
                        loadedImageList?.add(this@apply)
                    }
                }
            }
        }
        action!!()
    }

    private fun URL.getBitmap(): Bitmap? {
        val stream = openStream()

        return try {
            BitmapFactory.decodeStream(stream)
        } catch (e: IOException) {
            null
        }
    }
}