package com.eng.audieventi.model

import com.eng.audieventi.enumeration.Result

interface RequestResult {
    val result: Result
    val resultCode: String
    val resultMessage: String
}