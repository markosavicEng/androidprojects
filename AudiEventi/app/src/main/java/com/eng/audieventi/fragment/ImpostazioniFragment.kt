package com.eng.audieventi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eng.audieventi.R
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.DataGlobal

class ImpostazioniFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_impostazioni, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews(view)
    }

    private fun setViews(view: View) {
        val pageHeaderTextView = view.findViewById<TextView>(R.id.pageHeaderTextView)

        pageHeaderTextView.text = ApiJsonData.langsJsonDataMap["SIDE_MENU_GUEST_SETTINGS"]
    }
}