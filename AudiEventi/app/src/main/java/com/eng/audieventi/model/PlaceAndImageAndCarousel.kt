package com.eng.audieventi.model

import android.graphics.Bitmap

class PlaceAndImageAndCarousel(
    val place: Place,
    val image: Bitmap?,
    val carousel: ArrayList<Bitmap>?
)