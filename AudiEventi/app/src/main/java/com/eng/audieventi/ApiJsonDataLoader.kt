package com.eng.audieventi

import android.os.AsyncTask
import com.eng.audieventi.singleton.ApiJsonData
import com.eng.audieventi.singleton.UrlData
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class ApiJsonDataLoader(val dataUrl: String) : AsyncTask<String, String, String>() {
    override fun doInBackground(vararg params: String?): String {
        val json: String
        val connection = URL(params[0]).openConnection() as HttpURLConnection
        try {
            connection.connect()
            json = connection.inputStream.use {
                it.reader().use {
                    reader -> reader.readText()
                }
            }
        } finally {
            connection.disconnect()
        }
        return json
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

        if (dataUrl == UrlData.getLangsUrl()) {
            val jsonObject = JSONObject(result!!)

            for (key in jsonObject.keys()) {
                ApiJsonData.langsJsonDataMap[key] = jsonObject.getString(key)
            }
        }
    }
}