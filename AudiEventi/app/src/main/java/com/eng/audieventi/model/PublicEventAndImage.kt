package com.eng.audieventi.model

import android.graphics.Bitmap

class PublicEventAndImage(
    val publicEvent: PublicEvent,
    val image: Bitmap?
)