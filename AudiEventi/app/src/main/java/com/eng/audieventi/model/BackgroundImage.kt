package com.eng.audieventi.model

data class BackgroundImage(
    val id: String,
    val href: String,
    val meta: Meta
)

data class Meta(
    val alt: String,
    val title: String,
    val width: Int,
    val height: Int
)