package com.eng.audieventi.model

import android.graphics.Bitmap

class PremiumEventAndImage(
    val premiumEvent: PremiumEvent,
    val imageList: ArrayList<Bitmap>?
)