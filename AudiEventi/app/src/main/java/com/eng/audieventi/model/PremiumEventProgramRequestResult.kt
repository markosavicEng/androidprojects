package com.eng.audieventi.model

import com.eng.audieventi.enumeration.Result

data class PremiumEventProgramRequestResult(
    override val result: Result,
    override val resultCode: String,
    override val resultMessage: String,

    val data: PremiumEvent
) : RequestResult