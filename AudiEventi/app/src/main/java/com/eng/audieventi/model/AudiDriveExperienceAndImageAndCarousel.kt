package com.eng.audieventi.model

import android.graphics.Bitmap

class AudiDriveExperienceAndImageAndCarousel(
    val audiDriveExperience: AudiDriveExperience,
    val image: Bitmap?,
    val carousel: ArrayList<Bitmap>?
)