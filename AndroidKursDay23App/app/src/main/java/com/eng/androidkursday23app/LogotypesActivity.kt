package com.eng.androidkursday23app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

data class BrandAndImage(val brand: String, val image: Int)

class LogotypesActivity : AppCompatActivity() {

    var logoArray: ArrayList<Int> = arrayListOf()
    var brandArray: ArrayList<String> = arrayListOf()
    var isSearch = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotypes)

        DataGlobal.filteredCars.clear()

        addLogosToArray()
        addBrandsToArray()
        addBrandsAndImagesToArray()

        val recyclerView = findViewById<RecyclerView>(R.id.logotypeRecyclerView)
        val searchBarEditText = findViewById<EditText>(R.id.searchBarEditText)

        findViewById<Button>(R.id.searchBarButton).setOnClickListener {
            if (!searchBarEditText.text.toString().equals("") && DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in (it.marka + it.model).toLowerCase().replace(" ", "")}.isNotEmpty()){
                DataGlobal.filteredCars.clear()
                isSearch = true
//                if (DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in it.marka.toLowerCase().replace(" ", "") }.isNotEmpty()) {
//                    DataGlobal.filteredCars = DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in it.marka.toLowerCase().replace(" ", "") } as ArrayList<Car>
//                } else if (DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in it.model.toLowerCase().replace(" ", "") }.isNotEmpty()) {
//                    DataGlobal.filteredCars = DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in it.model.toLowerCase().replace(" ", "") } as ArrayList<Car>
//                } else {
//                    DataGlobal.filteredCars = DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in (it.marka.toLowerCase().replace(" ", "") + it.model.toLowerCase().replace(" ", "")) } as ArrayList<Car>
//                }
                DataGlobal.filteredCars = DataGlobal.cars.filter { searchBarEditText.text.toString().toLowerCase().replace(" ", "") in (it.marka.toLowerCase().replace(" ", "") + it.model.toLowerCase().replace(" ", "")) } as ArrayList<Car>
                recyclerView.adapter = modelCellAdapter(logoArray, brandArray)
                recyclerView.layoutManager = GridLayoutManager(this, 2)
            } else {
                Toast.makeText(this, "Search was unsuccessfull!", Toast.LENGTH_SHORT).show()
            }
        }

        recyclerView.adapter = logotypeCellAdapter(logoArray, brandArray)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
    }

    override fun onBackPressed() {
        finish()
        if(isSearch)
            startActivity(Intent(this, LogotypesActivity::class.java))
        else
            startActivity(Intent(this, MainActivity::class.java))
    }

    fun addLogosToArray() {
        logoArray.add(R.drawable.alfa_romeo)
        logoArray.add(R.drawable.audi)
        logoArray.add(R.drawable.citroen)
        logoArray.add(R.drawable.dacia)
        logoArray.add(R.drawable.fiat)
        logoArray.add(R.drawable.ford)
        logoArray.add(R.drawable.honda)
        logoArray.add(R.drawable.hyundai)
        logoArray.add(R.drawable.infiniti)
        logoArray.add(R.drawable.isuzu)
        logoArray.add(R.drawable.jeep)
        logoArray.add(R.drawable.lada)
        logoArray.add(R.drawable.mazda)
        logoArray.add(R.drawable.mercedes_benz)
        logoArray.add(R.drawable.mini)
        logoArray.add(R.drawable.mitsubishi)
        logoArray.add(R.drawable.nissan)
        logoArray.add(R.drawable.opel)
        logoArray.add(R.drawable.peugeot)
        logoArray.add(R.drawable.renault)
        logoArray.add(R.drawable.seat)
        logoArray.add(R.drawable.skoda)
        logoArray.add(R.drawable.smart)
        logoArray.add(R.drawable.subaru)
        logoArray.add(R.drawable.suzuki)
        logoArray.add(R.drawable.volkswagen)
        logoArray.add(R.drawable.volvo)
    }

    fun addBrandsToArray() {
        brandArray.add("Alfa Romeo")
        brandArray.add("Audi")
        brandArray.add("Citroen")
        brandArray.add("Dacia")
        brandArray.add("Fiat")
        brandArray.add("Ford")
        brandArray.add("Honda")
        brandArray.add("Hyundai")
        brandArray.add("Infiniti")
        brandArray.add("Isuzu")
        brandArray.add("Jeep")
        brandArray.add("Lada")
        brandArray.add("Mazda")
        brandArray.add("Mercedes-Benz")
        brandArray.add("Mini")
        brandArray.add("Mitsubishi")
        brandArray.add("Nissan")
        brandArray.add("Opel")
        brandArray.add("Peugeot")
        brandArray.add("Renault")
        brandArray.add("Seat")
        brandArray.add("Skoda")
        brandArray.add("Smart")
        brandArray.add("Subaru")
        brandArray.add("Suzuki")
        brandArray.add("Volkswagen")
        brandArray.add("Volvo")
    }

    fun addBrandsAndImagesToArray() {
        DataGlobal.brandsAndImages.clear()
        DataGlobal.brandsAndImages.add(BrandAndImage("Alfa Romeo", R.drawable.alfa_romeo_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Audi", R.drawable.audi_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Citroen", R.drawable.citroen_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Dacia", R.drawable.dacia_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Fiat", R.drawable.fiat_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Ford", R.drawable.ford_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Honda", R.drawable.honda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Hyundai", R.drawable.hyundai_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Infiniti", R.drawable.infiniti_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Isuzu", R.drawable.isuzu_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Jeep", R.drawable.jeep_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Lada", R.drawable.lada_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mazda", R.drawable.mazda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mercedes-Benz", R.drawable.mercedes_benz_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mini", R.drawable.mini_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Mitsubishi", R.drawable.mitsubishi_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Nissan", R.drawable.nissan_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Opel", R.drawable.opel_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Peugeot", R.drawable.peugeot_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Renault", R.drawable.renault_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Seat", R.drawable.seat_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Skoda", R.drawable.skoda_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Smart", R.drawable.smart_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Subaru", R.drawable.subaru_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Suzuki", R.drawable.suzuki_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Volkswagen", R.drawable.volkswagen_car))
        DataGlobal.brandsAndImages.add(BrandAndImage("Volvo", R.drawable.volvo_car))
    }
}