package com.eng.androidkursday23app

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class logotypeCellAdapter(val logoArray: ArrayList<Int>, val brandArray: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
//        DataGlobal.filteredCars.clear()
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.logotype_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<ImageView>(R.id.cellImage).setImageResource(logoArray[position])
        holder.view.findViewById<TextView>(R.id.cellText).text = brandArray[position]

        holder.view.setOnClickListener {
            val intent = Intent(holder.view.context, ModelsActivity::class.java)
            intent.putExtra("BRAND", brandArray[position])

            intent.putExtra("LOGOARRAY", logoArray)
            intent.putExtra("BRANDARRAY", brandArray)

            DataGlobal.selectedBrandImage = logoArray[position]
            holder.view.context.startActivity(intent)
//            (holder.view.context as Activity).finish()
        }
    }

    override fun getItemCount(): Int {
        return logoArray.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view)