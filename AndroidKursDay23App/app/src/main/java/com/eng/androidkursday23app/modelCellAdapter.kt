package com.eng.androidkursday23app

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class modelCellAdapter(val logoArray: ArrayList<Int>, val brandArray: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder>() {

    var image: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.model_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        image = DataGlobal.brandsAndImages.filter { it.brand in DataGlobal.filteredCars[position].marka }[0].image
        holder.view.findViewById<ImageView>(R.id.modelImage).setImageResource(image)
        holder.view.findViewById<TextView>(R.id.modelNameText).text = DataGlobal.filteredCars[position].marka + " " + DataGlobal.filteredCars[position].model
        holder.view.findViewById<TextView>(R.id.priceText).text = DataGlobal.filteredCars[position].cena + " \u20ac"
        // drugi nacin za postavljanje logotipa na ModelsActivity, uz prosledjeni context
//        (context as AppCompatActivity).findViewById<ImageView>(R.id.brandLogotypeImage).setImageResource(DataGlobal.selectedBrandImage)

        holder.view.setOnClickListener {
            //image = DataGlobal.brandsAndImages.filter { it.brand in DataGlobal.filteredCars[position].marka }[0].image
            val intent = Intent(holder.view.context, ModelDetailsActivity::class.java)
            intent.putExtra("BRAND", DataGlobal.filteredCars[position].marka)
            intent.putExtra("MODEL", DataGlobal.filteredCars[position].model)
            intent.putExtra("ENGINE", DataGlobal.filteredCars[position].motor)
            intent.putExtra("PACKAGE", DataGlobal.filteredCars[position].paketOpreme)
            intent.putExtra("WEIGHT", DataGlobal.filteredCars[position].tezinaPraznogVozila)
            //intent.putExtra("IMAGE", image)

            DataGlobal.selectedBrandImage = logoArray[brandArray.indexOf(DataGlobal.filteredCars[position].marka.trim())]
            holder.view.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return DataGlobal.filteredCars.count()
    }

}