package com.eng.androidkursday23app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet

class ModelDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model_details)

        val selectedCar = DataGlobal.filteredCars.filter { it.marka == intent.getStringExtra("BRAND") && it.model == intent.getStringExtra("MODEL") && it.motor == intent.getStringExtra("ENGINE") && it.paketOpreme == intent.getStringExtra("PACKAGE") && it.tezinaPraznogVozila == intent.getStringExtra("WEIGHT")}

        for (i in DataGlobal.brandsAndImages) {
            if (i.brand in intent.getStringExtra("BRAND")!!) {
                findViewById<ImageView>(R.id.modelDetailsModelImage).setImageResource(i.image)
            }
        }

        setViews(selectedCar[0])

        val showDetails = findViewById<ConstraintLayout>(R.id.showDetails)
        val hideDetails = findViewById<ConstraintLayout>(R.id.hideDetails)
        val hiddenAttributes = findViewById<ConstraintLayout>(R.id.hiddenAttributes)

        findViewById<ImageView>(R.id.modelDetailsBrandLogotype).setImageResource(DataGlobal.selectedBrandImage)
        //findViewById<ImageView>(R.id.modelDetailsModelImage).setImageResource(intent.getIntExtra("IMAGE", 0))

        showDetails.setOnClickListener {
            hiddenAttributes.visibility = View.VISIBLE
            showDetails.visibility = View.GONE
            hideDetails.visibility = View.VISIBLE
        }

        hideDetails.setOnClickListener {
            hiddenAttributes.visibility = View.GONE
            showDetails.visibility = View.VISIBLE
            hideDetails.visibility = View.GONE
        }
    }

    fun setViews(selectedCar: Car) {
        findViewById<TextView>(R.id.modelDetailsMarka).text = selectedCar.marka
        findViewById<TextView>(R.id.modelDetailsModel).text = selectedCar.model
        findViewById<TextView>(R.id.modelDetailsMotor).text = selectedCar.motor
        findViewById<TextView>(R.id.modelDetailsPaketOpreme).text = selectedCar.paketOpreme
        findViewById<TextView>(R.id.modelDetailsCena).text = selectedCar.cena + " \u20ac"
        findViewById<TextView>(R.id.modelDetailsRasporedCilindara).text = selectedCar.rasporedCilindara
        findViewById<TextView>(R.id.modelDetailsBrojVentila).text = selectedCar.brojVentila
        findViewById<TextView>(R.id.modelDetailsPrecnikHodKlipa).text = selectedCar.precnikHodKlipa
        findViewById<TextView>(R.id.modelDetailsTipUbrizgavanja).text = selectedCar.tipUbrizgavanja
        findViewById<TextView>(R.id.modelDetailsSistemOtvaranjaVentila).text = selectedCar.sistemOtvaranjaVentila
        findViewById<TextView>(R.id.modelDetailsTurbo).text = selectedCar.turbo
        findViewById<TextView>(R.id.modelDetailsZapreminaMotora).text = selectedCar.zapreminaMotora
        findViewById<TextView>(R.id.modelDetailsKW).text = selectedCar.KW
        findViewById<TextView>(R.id.modelDetailsKS).text = selectedCar.KS
        findViewById<TextView>(R.id.modelDetailsSnagaPriObrtajima).text = selectedCar.snagaPriObrtajima
        findViewById<TextView>(R.id.modelDetailsObrtniMoment).text = selectedCar.obrtniMoment
        findViewById<TextView>(R.id.modelDetailsObrtniMomentPriObrtajima).text = selectedCar.obrtniMomentPriObrtajima
        findViewById<TextView>(R.id.modelDetailsStepenKompresije).text = selectedCar.stepenKompresije
        findViewById<TextView>(R.id.modelDetailsTipMenjaca).text = selectedCar.tipMenjaca
        findViewById<TextView>(R.id.modelDetailsBrojStepeniPrenosa).text = selectedCar.brojStepeniPrenosa
        findViewById<TextView>(R.id.modelDetailsPogon).text = selectedCar.pogon
        findViewById<TextView>(R.id.modelDetailsDuzina).text = selectedCar.duzina
        findViewById<TextView>(R.id.modelDetailsSirina).text = selectedCar.sirina
        findViewById<TextView>(R.id.modelDetailsVisina).text = selectedCar.visina
        findViewById<TextView>(R.id.modelDetailsMedjuosovinskoRastojanje).text = selectedCar.medjuosovinskoRastojanje
        findViewById<TextView>(R.id.modelDetailsTezinaPraznogVozila).text = selectedCar.tezinaPraznogVozila
        findViewById<TextView>(R.id.modelDetailsMaksimalnaDozvoljenaTezina).text = selectedCar.maksimalnaDozvoljenaTezina
        findViewById<TextView>(R.id.modelDetailsZapreminaRezervoara).text = selectedCar.zapreminaRezervoara
        findViewById<TextView>(R.id.modelDetailsZapreminaPrtljaznika).text = selectedCar.zapreminaPrtljaznika
        findViewById<TextView>(R.id.modelDetailsMaksZapreminaPrtljaznika).text = selectedCar.maksZapreminaPrtljaznika
        findViewById<TextView>(R.id.modelDetailsDozvoljenTovar).text = selectedCar.dozvoljenTovar
        findViewById<TextView>(R.id.modelDetailsDozvoljenoOpterecenjeKrova).text = selectedCar.dozvoljenoOpterecenjeKrova
        findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceBK).text = selectedCar.dozvoljenaTezinaPrikoliceBK
        findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceSK12).text = selectedCar.dozvoljenaTezinaPrikoliceSK12
        findViewById<TextView>(R.id.modelDetailsDozvoljenaTezinaPrikoliceSK8).text = selectedCar.dozvoljenaTezinaPrikoliceSK8
        findViewById<TextView>(R.id.modelDetailsOpterecenjeKuke).text = selectedCar.opterecenjeKuke
        findViewById<TextView>(R.id.modelDetailsRadijusOkretanja).text = selectedCar.radijusOkretanja
        findViewById<TextView>(R.id.modelDetailsTragTockovaNapred).text = selectedCar.tragTockovaNapred
        findViewById<TextView>(R.id.modelDetailsTragTockovaNazad).text = selectedCar.tragTockovaNazad
        findViewById<TextView>(R.id.modelDetailsMaksimalnaBrzina).text = selectedCar.maksimalnaBrzina
        findViewById<TextView>(R.id.modelDetailsUbrzanje0_100).text = selectedCar.ubrzanje0_100
        findViewById<TextView>(R.id.modelDetailsUbrzanje0_200).text = selectedCar.ubrzanje0_200
        findViewById<TextView>(R.id.modelDetailsUbrzanje80_120FinalniStepen).text = selectedCar.ubrzanje80_120FinalniStepen
        findViewById<TextView>(R.id.modelDetailsZaustavniPut100).text = selectedCar.zaustavniPut100
        findViewById<TextView>(R.id.modelDetailsVremeZa400m).text = selectedCar.vremeZa400m
        findViewById<TextView>(R.id.modelDetailsPotrosnjaGrad).text = selectedCar.potrosnjaGrad
        findViewById<TextView>(R.id.modelDetailsPotrosnjaVGrada).text = selectedCar.potrosnjaVGrada
        findViewById<TextView>(R.id.modelDetailsEmisijaCO2).text = selectedCar.emisijaCO2
        findViewById<TextView>(R.id.modelDetailsKatalizator).text = selectedCar.katalizator
        findViewById<TextView>(R.id.modelDetailsDimenzijePenumatika).text = selectedCar.dimenzijePneumatika
        findViewById<TextView>(R.id.modelDetailsPrednjeOpruge).text = selectedCar.prednjeOpruge
        findViewById<TextView>(R.id.modelDetailsZadnjeOpruge).text = selectedCar.zadnjeOpruge
        findViewById<TextView>(R.id.modelDetailsPrednjiStabilizator).text = selectedCar.prednjiStabilizator
        findViewById<TextView>(R.id.modelDetailsZadnjiStabilizator).text = selectedCar.zadnjiStabilizator
        findViewById<TextView>(R.id.modelDetailsGarancijaKorozija).text = selectedCar.garancijaKorozija
        findViewById<TextView>(R.id.modelDetailsGarancijaMotor).text = selectedCar.garancijaMotor
        findViewById<TextView>(R.id.modelDetailsEuroNCAP).text = selectedCar.euroNCAP
        findViewById<TextView>(R.id.modelDetailsEuroNCAPZvezdice).text = selectedCar.euroNCAPZvezdice
        findViewById<TextView>(R.id.modelDetailsGorivo).text = selectedCar.gorivo
        findViewById<TextView>(R.id.modelDetailsBrojVrata).text = selectedCar.brojVrata
        findViewById<TextView>(R.id.modelDetailsBrojSedista).text = selectedCar.brojSedista
    }
}